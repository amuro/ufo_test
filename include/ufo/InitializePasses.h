#ifndef UFO_INITIALIZEPASSES_H
#define UFO_INITIALIZEPASSES_H

namespace llvm {
  class PassRegistry;
  void initializeShadowMemPass (PassRegistry&);
  void initializeErrorExitPass (PassRegistry&);
  void initializeExitReturnPass (PassRegistry&);
  void initializeNondetInitPass (PassRegistry&);
  void initializeReturnGotoPass (PassRegistry&);
  void initializeWTOPassPass (PassRegistry&);
  
  void initializeTopoPass (PassRegistry&);
  void initializeLBEPass (PassRegistry&);
  void initializeLawiPass (PassRegistry&);

  void initializeAlwaysInlineMarkPass (PassRegistry&);
  void initializeNameValuesPass (PassRegistry&);
}
#endif

