#ifndef _STATS__HPP_
#define _STATS__HPP_

#include <map>

#include <sys/time.h>
#include <sys/resource.h>

#include <boost/foreach.hpp>
#define foreach BOOST_FOREACH

#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/Format.h"

namespace
{
  class Stopwatch
  {
  private:
    long started;
    long finished;
    long timeElapsed;
    
    long systemTime () const
    {
      struct rusage ru;
      getrusage (RUSAGE_SELF, &ru);
      long r = ru.ru_utime.tv_sec * 1000000L + ru.ru_utime.tv_usec;
      return r;
      
    }
    
  public:
    Stopwatch () { start (); }
    
    void start () 
    {
      started = systemTime ();
      finished = -1;
      timeElapsed = 0;
    }

    void stop () 
    {
      if (finished < started)
	{
	  finished = systemTime ();
	}
      
    }

    void resume () 
    {
      if (finished >= started)
	{
	  timeElapsed += finished - started;
	  started = systemTime ();
	  finished = -1;
	}	  
    }

    long getTimeElapsed () const
    {
      if (finished < started) return timeElapsed + systemTime () - started;
      return timeElapsed + finished - started;
    }
    

    void Print (std::ostream &out) const
    {      
      long time = getTimeElapsed ();
      long h = time/3600000000L;
      long m = time/60000000L - h*60;
      float s = ((float)time/1000000L) - m*60 - h*3600;

      if (h > 0) out << h << "h";
      if (m > 0) out << m << "m";
      out << s << "s";
    }

    void Print (llvm::raw_ostream &out) const
    {
      long time = getTimeElapsed ();
      long h = time/3600000000L;
      long m = time/60000000L - h*60;
      float s = ((float)time/1000000L) - m*60 - h*3600;

      if (h > 0) out << h << "h ";
      if (m > 0) out << m << "m ";
      out << llvm::format ("%.2f", s) << "s";

    }

    double toSeconds(){
      double time = ((double) getTimeElapsed () / 1000000) ;
      return time;
    }

  };


  /** Computes running average */
  class Averager
  {
  private:
    size_t count;
    double avg;
    
  public :
    Averager () : count(0), avg (0) {}
    
    double add (double k)
    {
      avg += (k - avg)/(++count);
      return avg;
    }

    void Print (std::ostream &out) const { out << avg; }

    void Print (llvm::raw_ostream &out) const
    {
      out << llvm::format ("%.2f", avg);
    }
  };
    
}



namespace ufo
{
  inline std::ostream &operator<< (std::ostream &OS, const Stopwatch &sw)
  {
    sw.Print (OS);
    return OS;
  }

  inline llvm::raw_ostream &operator<< (llvm::raw_ostream &OS, 
					const Stopwatch &sw)
  {
    sw.Print (OS);
    return OS;
  }

  inline std::ostream &operator<< (std::ostream &OS, const Averager &av)
  {
    av.Print (OS);
    return OS;
  }

  inline llvm::raw_ostream &operator<< (llvm::raw_ostream &OS, 
					const Averager &av)
  {
    av.Print (OS);
    return OS;
  }

  
  class Stats 
  {
  private:
    static std::map<std::string,unsigned> counters;
    static std::map<std::string,Stopwatch> sw;
    static std::map<std::string,Averager> av;

    
  public:
    static unsigned  get (const std::string &n);
    static double avg (const std::string &n, double v);
    static unsigned uset (const std::string &n, unsigned v);

    static void count (const std::string &name);
    
    static void start (const std::string &name);
    static void stop (const std::string &name);
    static void resume (const std::string &name);

    /** Outputs all statistics to std output */
    static void Print (std::ostream &OS);
    static void Print (llvm::raw_ostream &OS);
    static void PrintBrunch (llvm::raw_ostream &OS);
  };



  /** 
      Usage: add 
         auto_timer X("foo.bar");
      at the beginning of a block (i.e., function body, conditional, etc)
      to measure the time it takes to execute.
   */
  // class auto_timer 
  // {
  // private:
  //   std::string n;
  // public:
  //   auto_timer (const std::string &name) : n(name) { Stats::resume (n); }
  //   ~auto_timer () { Stats::stop (n); }
  // };
  
}

  

#endif
