#ifndef _LBE__H_
#define _LBE__H_

#include <map>
#include <typeinfo>
#include <string>
#include "llvm/Support/raw_ostream.h"
#include "llvm/ADT/BitVector.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Pass.h"
#include "llvm/Function.h"
#include "llvm/Instructions.h"
#include "llvm/Constants.h"
#include "llvm/Analysis/Dominators.h"

#include <boost/shared_ptr.hpp>
#include <boost/foreach.hpp>
#define forall BOOST_FOREACH

#include "ufo/Cpg/Topo.hpp"

namespace llvm
{
  class SEdge;
  class CutPoint;
  class LBE;
  typedef boost::shared_ptr<CutPoint> CutPointPtr;
  typedef boost::shared_ptr<SEdge> SEdgePtr;
  typedef SmallVector<boost::shared_ptr<SEdge>,3 >::const_iterator edgeIterator;
  typedef std::vector<CutPointPtr> CutPointVector;
  

  class SEdge 
  {
  protected:
    // basic blocks on the edge
    std::vector<const BasicBlock*> bbs; 
    // source cut point
    const CutPointPtr src;				
    // -- dest cut point
    const CutPointPtr dst;
  public:
    SEdge (const CutPointPtr isrc, 
	   const CutPointPtr idst) : src(isrc), dst (idst) {}

    const CutPointPtr getSrc() const { return src; }
    const CutPointPtr getDst() const { return dst; }


    typedef std::vector<const BasicBlock*>::iterator iterator;
    typedef std::vector<const BasicBlock*>::const_iterator const_iterator;
    const_iterator begin() const { return bbs.begin();}
    const_iterator end() const { return bbs.end();}
    iterator begin() { return bbs.begin();}
    iterator end() { return bbs.end();}

    size_t getSize() const { return bbs.size(); }

    std::string toString() const;
    
    friend class LBE;
  };

 
  class CutPoint
  {
  private:
    int id;	
    // -- BB of this cut point
    const BasicBlock* bb;				
    // -- successors and preds
    // XXX The choice of 3 is random
    SmallVector<boost::shared_ptr<SEdge>,3 > pred;  

  public:
    SmallVector<boost::shared_ptr<SEdge>,3 > succ;    
    CutPoint(int i, const BasicBlock* block) : id(i), bb(block) {}
    int getID() const { return id;}
    const BasicBlock* getBB() const {return bb;}

    std::string getNameStr () const { return getBB()->getNameStr(); }
    
    /**
     * Find metadata for the cutpoint by taking the metadata of the
     * first instruction that has it
     */
    MDNode *getMetadata (const char *Kind) const
    {
      for (BasicBlock::const_iterator it = bb->begin (), end = bb->end ();
      	   it != end; ++it)
	if (MDNode *N = it->getMetadata (Kind)) return N;

      return NULL;
    }
    
    const DebugLoc *getDebugLoc () const
    {
      for (BasicBlock::const_iterator it = bb->begin (), end = bb->end ();
      	   it != end; ++it)
	{
	  const DebugLoc *loc = &it->getDebugLoc ();
	  if (! loc->isUnknown ()) return loc;
	}
      return NULL;
    }
    

    edgeIterator succBegin() const { return succ.begin();}
    edgeIterator succEnd() const { return succ.end();}
    edgeIterator predBegin() const { return pred.begin();}
    edgeIterator predEnd() const { return pred.end();}
    
    size_t succSize() const { return succ.size();}
    size_t predSize() const { return pred.size();}
    
    SEdgePtr getSucc (size_t index) const 
    {
      assert ((index < succ.size()) && "index out of bounds");
      return succ[index];
    }
    
    SEdgePtr getPred (size_t index) const
    {
      assert ((index < pred.size()) && "index out of bounds");
      return pred[index];
    }

    /** returns an SEdge between current CutPoint and the destination.
	Let r = getSEdge (x). Then r.get() == 0 iff no edge to x is found 
    **/
    edgeIterator findSEdge (const CutPoint &dst) const
    {
      for (edgeIterator i = succBegin(), e = succEnd(); i != e; ++i)
	if (*(*i)->getDst () == dst) return i;
      return succEnd ();
    }      


    //operator =
    /* CutPoint& operator= (const CutPoint &rhs) { */
    /*   id = rhs.id; */
    /*   bb = rhs.bb; */
    /*   // XXX What about succ and pred? */
    /*   return *this; */
    /* } */

    bool operator== (const CutPoint &cp) const
    {
      return id == cp.id;
    }
    bool operator!= (const CutPoint &cp) const
    {
      return !(*this == cp);
    }
    

    // give LBE access to private and protected members
    friend class LBE;
  };
  
   
  struct LBE : public ModulePass
  {
    static char ID;
    Function* F;

    // -- map from function pointer to a vector of its cutpoints
    std::map<const Function*, CutPointVector> cutPts;

    // -- map from basic blocks to cutpoints
    DenseMap<const BasicBlock*,CutPointPtr> bb2cp; 

    typedef DenseMap<const BasicBlock*,BitVector> cutptsmap_type;
    // -- map from bbs to bitvectors
    cutptsmap_type fwdCutPts; 
    cutptsmap_type bwdCutPts;

    LBE () : ModulePass (ID) { errs () << "\n Creating LBE pass \n";}	
    void computeCutPts(Function &F);
	
    void computeFwdCutPts(Function &F);
    void computeBwdCutPts (Function &F);
  	
    void computeSEdges(Function& F);
    void addToEdge(CutPointPtr src, CutPointPtr dst, const BasicBlock* bb);
 

    const CutPointVector &getFunctionCutPts (const Function &F) const
      {
	std::map<const Function*,CutPointVector>::const_iterator it = 
	  cutPts.find (&F);
	assert (it != cutPts.end ());
	return it->second;
      }
    
   
    bool runOnModule (Module &M);
    virtual void getAnalysisUsage (AnalysisUsage &AU) const 
    {
      AU.addRequired<UnifyFunctionExitNodes>();
      AU.addRequired<Topo>();
      AU.setPreservesAll ();
    }


    bool isCutPoint(const BasicBlock* bb) const { return bb2cp.count (bb) > 0; }
    
    /** returns true if cp can reach bb without going through other
	cutpoints */
    bool canImmReach (const CutPoint &cp, const BasicBlock* bb) const
    {
      if (cp.bb == bb) return true;
      
      int id = cp.getID ();
      cutptsmap_type::const_iterator it = bwdCutPts.find (bb);
      assert (it != bwdCutPts.end ());
      
      int size = it->second.size ();
      if (size == 0 || id >= size) return false;
      return (it->second) [id];
    }
    
       
    // -- Gives a LBE path from beginning of Function F
    // -- to return BB. It assumes that there are no dead ends
    // -- in CFG, except for 1 return location, 
    // -- and a possible error cutpoint
    void getRandFPath(std::vector<SEdgePtr>& path,
                      Function* F) const;
  };



  // Boost.Foreach stuff
  inline edgeIterator range_begin (CutPoint &cp)
  {
    return cp.succBegin ();
  }
  inline edgeIterator range_begin (const CutPoint &cp)
  {
    return cp.succBegin ();
  }

  inline edgeIterator range_end (CutPoint &cp)
  {
    return cp.succEnd ();
  }
  inline edgeIterator range_end (const CutPoint  &cp)
  {
    return cp.succEnd ();
  }
}

namespace boost
{
  template<>
  struct range_mutable_iterator<llvm::CutPoint>
  {
    typedef llvm::edgeIterator type;
  };
  
  template<>
  struct range_const_iterator <llvm::CutPoint>
  {
    typedef llvm::edgeIterator type;
  };
}



#endif
