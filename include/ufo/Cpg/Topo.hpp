#ifndef __TOPO__H_
#define __TOPO__H_

#include "llvm/Pass.h"
#include "llvm/Function.h"
#include "llvm/Transforms/Utils/UnifyFunctionExitNodes.h"
#include "llvm/ADT/DenseMap.h"
#include "llvm/ADT/SmallPtrSet.h"

namespace llvm
{
  struct Topo: public ModulePass
  {

    typedef std::pair<const BasicBlock*,int> bb2int;
    typedef SmallPtrSet<const BasicBlock*, 3> ptrSet;
    typedef std::map<const BasicBlock*, ptrSet > bb2bbs;
    typedef std::pair<const BasicBlock*, ptrSet > bb2bbsPair;

    static char ID;
    // basic blocks in topological order
    std::map<Function*, std::vector<const BasicBlock*> > topo;		
	
    //Assuming that a node can have at most (but can be more) 3 back edges
    //for SmallPtrSet sake.
    std::map<Function*, bb2bbs> backEdges;
		
    Topo () : ModulePass (ID) {
      errs () << "\n Creating Topo pass \n;";
    }

    bool isBackEdge(Function* F, const BasicBlock* par, const BasicBlock* child)
    {
      bb2bbs::iterator it = backEdges[F].find(par);
      if (it == backEdges[F].end()) return false;
      return it->second.count (child);
    }


    void findBackEdges(Function &F);
		
    virtual bool runOnModule (Module &M);
    virtual void getAnalysisUsage (AnalysisUsage &AU) const
    {
      AU.setPreservesAll ();
      AU.addRequired<UnifyFunctionExitNodes>();
    }


    // -- iterators for vector topo
    typedef std::vector<const BasicBlock*>::const_iterator const_iterator;

    typedef std::vector<const BasicBlock*>::const_reverse_iterator 
      const_reverse_iterator;

    const_iterator begin(Function* F)  { return topo[F].begin(); }
    const_iterator end(Function* F)  { return topo[F].end(); }
    const_reverse_iterator rbegin(Function* F)  { return topo[F].rbegin(); }
    const_reverse_iterator rend(Function* F)  { return topo[F].rend(); }
  };
}

#endif
