#ifndef __UFO__HPP_
#define __UFO__HPP_

#include "llvm/Support/raw_ostream.h"
#include "llvm/Pass.h"
#include "llvm/Type.h"
#include "llvm/Function.h"
#include "llvm/Instructions.h"
#include "llvm/Constants.h"
#include "llvm/BasicBlock.h"

#include <boost/logic/tribool.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/foreach.hpp>

#include <gmpxx.h>

#include "mathsat.h"

#include "ufo/Expr.hpp"
#include "ufo/Stats.hpp"

#include "ufo/UfoCL.hpp"

using namespace boost;
using namespace llvm;
using namespace ufo;

BOOST_TRIBOOL_THIRD_STATE(maybe)

#define forall BOOST_FOREACH
#define rev_forall BOOST_REVERSE_FOREACH

namespace ufo
{
  double getUsageTime ();

  typedef std::pair<int,int> IntPair;
}

/** print things into raw_ostream */
template <typename T>
inline llvm::raw_ostream &operator<<(llvm::raw_ostream &OS, const T &v)
{
  OS << boost::lexical_cast<std::string> (v);
  return OS;
}


namespace llvm
{
  using namespace boost;
  
  inline llvm::raw_ostream &operator<<(llvm::raw_ostream &OS, boost::tribool v)
  {
    OS << lexical_cast<std::string> (v);
    return OS;
  }

  inline llvm::raw_ostream &operator<<
  (llvm::raw_ostream &OS, bool (*)(boost::tribool, 
				   boost::logic::detail::indeterminate_t))
  {
    OS << lexical_cast<std::string> (tribool(indeterminate));
    return OS;
  }
}


namespace expr
{
  inline llvm::raw_ostream &operator<<(llvm::raw_ostream &OS, const Expr &p)
  {
    OS << p.get ();
    return OS;
  }

  inline llvm::raw_ostream &operator<<(llvm::raw_ostream &OS, 
				       const ENode &n)
  {
    OS << boost::lexical_cast<std::string> (n);
    return OS;
  }
  
  
  using namespace llvm;
  template<> struct TerminalTrait<const BasicBlock*>
  {
    static inline void print (std::ostream &OS, const BasicBlock* s, 
			      int depth, bool brkt) 
    { 
        OS << s->getNameStr ();
    }
    static inline bool less (const BasicBlock* s1, const BasicBlock* s2)
    { return s1 < s2; }

    static inline bool equal_to (const BasicBlock *b1, const BasicBlock *b2)
    { return b1 == b2; }

    static inline size_t hash (const BasicBlock *b)
    {
      boost::hash<const BasicBlock *> hasher;
      return hasher (b);
    }
    
    
  };
  
  template<> struct TerminalTrait<const Value*>
  {
    static inline void print (std::ostream &OS, const Value* s, 
			      int depth, bool brkt) 
    {
      if (s->hasName ())
	OS << (isa<GlobalValue> (s) ? '@' : '%')
	   << s->getNameStr ();
      else
	{
	  std::string ssstr;
	  raw_string_ostream ss(ssstr);
	  ss <<  *dynamic_cast<const Instruction*>(s);
	  std::string str = ss.str();
      
	  int f = str.find_first_not_of(' ');
	  string s1 = str.substr(f);
	  f = s1.find_first_of(' ');
	  OS << s1.substr(0,f);
	}
    }
    static inline bool less (const Value* s1, const Value* s2)
    { return s1 < s2; }

    static inline bool equal_to (const Value *v1, const Value *v2)
    { return v1 == v2; }
    
    static inline size_t hash (const Value *v)
    {
      boost::hash<const Value*> hasher;
      return hasher (v);
    }
  };


  template <>
  struct TerminalTrait<mpz_class>
  {
    static inline void print (std::ostream &OS, const mpz_class &v, 
			      int depth, bool brkt)
    { OS << v; }
    
    static inline bool less(const mpz_class &v1, const mpz_class &v2)
    { return v1 < v2; }
    
    static inline bool equal_to (const mpz_class &v1, const mpz_class &v2)
    { return v1 == v2; }
    
    static inline size_t hash (const mpz_class &v)
    {
      std::string str = boost::lexical_cast<std::string> (v);
      boost::hash<std::string> hasher;
      return hasher (str);
    }
    
    


  };


  template <>
  struct TerminalTrait<mpq_class>
  {
    static inline void print (std::ostream &OS, const mpq_class &v, 
			      int depth, bool brkt)
    { OS << v; }
    
    static inline bool less(const mpq_class &v1, const mpq_class &v2)
    { return v1 < v2; }

    static inline bool equal_to (const mpq_class &v1, const mpq_class &v2)
    { return v1 == v2; }
    
    static inline size_t hash (const mpq_class &v)
    {
      std::string str = boost::lexical_cast<std::string> (v);
      boost::hash<std::string> hasher;
      return hasher (str);
    }    
  };
}


namespace expr
{
  template <typename T> 
  struct TerminalTrait<T*>
  {
    static inline void print (std::ostream &OS, const T *v, 
			      int depth, bool brkt)
    {
      OS << v;
    }

    static inline bool less(const T *v1, const T *v2)
    { return v1 < v2; }
  };
}



namespace ufo
{
  using namespace llvm;
  
  typedef expr::Terminal<mpq_class> MPQ;
  typedef expr::Terminal<mpz_class> MPZ;
  
  //typedef expr::Terminal<const llvm::ConstantInt*> CONST_INT;
  typedef expr::Terminal<const llvm::BasicBlock*> BB;
  typedef expr::Terminal<const llvm::Value*> VALUE;

  inline bool isBoolType (const Type *t) { return t->isIntegerTy (1); }

  inline bool isIntNumber (const Value *v)
  {
    return isa<ConstantInt> (v) || isa<ConstantPointerNull> (v);
  }

  /** Converts v to mpz_class. Assumes that v is signed */
  inline mpz_class toMpz (const APInt &v)
  {
    // Based on:
    // https://llvm.org/svn/llvm-project/polly/trunk/lib/Support/GICHelper.cpp
    // return v.getSExtValue ();

    APInt abs;
    abs = v.isNegative () ? v.abs () : v;
    
    const uint64_t *rawdata = abs.getRawData ();
    unsigned numWords = abs.getNumWords ();

    // TODO: Check if this is true for all platforms.
    mpz_class res;
    mpz_import(res.get_mpz_t (), numWords, 1, sizeof (uint64_t), 0, 0, rawdata);

    return v.isNegative () ? mpz_class(-res) : res;
  }
  
  inline mpz_class toMpz (const Value *v)
  {
    if (const ConstantInt *k = dyn_cast<ConstantInt> (v))
      return toMpz (k->getValue ());
    if (isa<ConstantPointerNull> (v)) return 0;
    
    assert (0 && "Not a number");
    return 0;
  }
  
  

}

namespace ufo
{
  using namespace expr;
  
  /** Given a sequence of expressions returns a Boolean expression
      that is true iff exactly one of the input expressions is true */
  template <typename R> 
  Expr exactlyOne (R in, ExprFactory &efac)
  {
    ExprVector res;
    Expr trueE = mk<TRUE> (efac);
    
    foreach (Expr x, in)
      {
	ExprVector subRes;
	subRes.push_back (x);
	foreach (Expr y, in) if (y != x) subRes.push_back (mk<NEG> (y));
	if (!subRes.empty ())
	  res.push_back (mknary<AND> (trueE, subRes.begin (), subRes.end ()));
      }
    return mknary<OR> (mk<FALSE>(efac), res.begin (), res.end ());
  }
}



#endif
