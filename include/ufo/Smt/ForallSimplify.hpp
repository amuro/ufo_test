#ifndef __FORALL_SIMPLIFY_HPP_
#define __FORALL_SIMPLIFY_HPP_
/**
 * Heuristics to elimiante universal (forall) quantifier.
 */

#include "ufo/ufo.hpp"

namespace ufo
{
  Expr forallElim (Expr e, ExprSet &vars);
  Expr forallSimplify (Expr e, ExprSet &vars);
  Expr forallHeuristic (Expr e, ExprSet &vars);
}


#endif
