#ifndef __EXPR_Z3_HPP_
#define __EXPR_Z3_HPP_

//#define Z3_DEBUG_NAMES 1
#include <map>

#include "ufo/ufo.hpp"
#include "ufo/Smt/Z3.hpp"
#include "ufo/Arg.hpp"

using namespace ufoz3;
using namespace expr;



namespace ufo
{
  namespace z3
  {
    struct TerminalAssert
    {
      template <typename emap_type, typename zmap_type>
      static Z3_ast marshal (Z3_context env, 
			     Expr e,
			     emap_type &eMap,
			     zmap_type &zMap)
      {
	errs () << "UNKNOWN EXPRESSION: " << *e << "\n";
	assert (0 && "Unreachable");
	exit (1);
      }

      template <typename zmap_type>    
      static Expr unmarshal (Z3_context ctx,
	                     Z3_ast z, 
			     ExprFactory &efac,
			     zmap_type &zMap)
      {
	errs () << "UNKNOWN EXPRESSION\n"
		<< Z3_ast_to_string (ctx, z) << "\n";
	assert (0 && "Unreachable");
	exit (1);
      }
    };    
			       


    /** Handle UFO-specific terminals */
    template <typename EC>
    struct TerminalUfo
    {
      template <typename emap_type, typename zmap_type>
      static Z3_ast marshal (Z3_context ctx, 
			     Expr e,
			     emap_type &eMap,
			     zmap_type &zMap)
      {
      
	if (isOpX<ASM>(e)){
	  Expr assum = e->left();
	  std::string svar;
	  
	  if(isOpX<ULONG>(assum))
            svar = "assum" + 
	      boost::lexical_cast<std::string>(assum.get());
	  else if (isOpX<NODE>(assum))
	    svar = "node_assum"  +
	      boost::lexical_cast<std::string,void*>(e.get());
	  else
	    svar = "arie_eq_assum"  +
	      boost::lexical_cast<std::string,void*>(e.get());
	    //assert (0 && "incorrect assumption");

          Z3_sort sort = Z3_mk_bool_sort (ctx);
	 
	  Z3_ast res = Z3_mk_const (ctx, 
				      Z3_mk_string_symbol (ctx, svar.c_str ()),
				      sort);
	  
	  // -- update reverse lookup map
	  zMap [Z3_get_ast_id (ctx, res)] = e;
	  // -- update forward lookup map
	  eMap[e] = res;
	  return res;
	}
	
	if (isOpX<VARIANT>(e))
	  {
	    int num = variant::variantNum(e);
	    Expr var = variant::mainVariant(e);
	    std::string svar;
	    svar = "v" + 
	      boost::lexical_cast<std::string,void*>(var.get()) +
	      "_" + boost::lexical_cast<std::string>(num);
#ifdef Z3_DEBUG_NAMES	    
	    svar = boost::lexical_cast<std::string>(*var) + 
	      "_" + boost::lexical_cast<std::string>(num);
#endif

                        
            Z3_sort sort;
	    if (isOpX<BB> (var))
		sort = Z3_mk_bool_sort (ctx);
	    else
	      {
		Expr exp = var;
		  
		if (isOp<VARIANT>(var)) exp = variant::mainVariant(var);
		  
		const Value* c = getTerm<const Value*> (exp);
		  
		assert (c != NULL && "can't be null");
 
		if (c->getType ()->isIntegerTy (1))
		  sort = Z3_mk_bool_sort (ctx);
		else
		  sort = ufocl::USE_INTS ? 
		    Z3_mk_int_sort (ctx) : Z3_mk_real_sort (ctx);
	      }
	    
	    Z3_ast res = Z3_mk_const (ctx, 
				      Z3_mk_string_symbol (ctx, svar.c_str ()),
				      sort);

	    // -- update reverse lookup map
	    zMap [Z3_get_ast_id (ctx, res)] = e;
	    // -- update forward lookup map
	    eMap[e] = res;
	    return res;
	  }

	// if (isOpX<CONST_INT>(e))
	//   {
	//     const ConstantInt* c = 
	//       dynamic_cast<const CONST_INT&> (e->op ()).get ();
	      
	//     return 
	//       Z3_mk_numeral(ctx, 
	// 		    c->getValue().toString(10, true).c_str(), 
	// 		    Z3_mk_real_sort (ctx));
	//   }


	// -- Symbolic Constant
	if (isOpX<VALUE>(e))
	  {
	    std::string svar = "x" + 
	      boost::lexical_cast<std::string,void*>(e.get());
	      
	    const Value* c = getTerm<const Value*> (e);

	    assert (c != NULL && "can't be null");

	    Z3_sort sort;
	    if (c->getType ()->isIntegerTy (1))
	      sort = Z3_mk_bool_sort (ctx);
	    else  
	      sort = 
		ufocl::USE_INTS ? Z3_mk_int_sort (ctx) : Z3_mk_real_sort (ctx);

	      

	    Z3_ast res = 
	      Z3_mk_const (ctx, 
			   Z3_mk_string_symbol (ctx, svar.c_str ()),
			   sort);

	    eMap[e] = res;
	    zMap[Z3_get_ast_id(ctx, res)] = e;
	    return res;
	  }

	if (isOpX<BB>(e) || isOpX<NODE>(e))
	  {
	    std::string svar = (isOpX<BB>(e) ? "BB" : "N" ) +
	      boost::lexical_cast<std::string,void*>(e.get());
            
	    Z3_ast res = 
	      Z3_mk_const (ctx, 
			   Z3_mk_string_symbol (ctx, svar.c_str ()),
			   Z3_mk_bool_sort (ctx));
	    eMap[e] = res;
	    zMap[Z3_get_ast_id(ctx, res)] = e;
	    return res;
	  }


	// if (isOpX<NODE_PAIR> (e)){
	//   const std::pair<Node*, Node*> np = dynamic_cast<const NODE_PAIR&>(e->op()).get();
	//   std::string svar = "NP(" + boost::lexical_cast<std::string>(np.first) + "," + 
	//                              boost::lexical_cast<std::string>(np.second) + ")";
	  
	//   Z3_ast res = 
	//       Z3_mk_const (ctx, 
	// 		   Z3_mk_string_symbol (ctx, svar.c_str ()),
	// 		   Z3_mk_bool_sort (ctx));

	  
	//   eMap[e] = res;
	//   zMap[Z3_get_ast_id(ctx, res)] = e;
	//   return res;
	// }
	
	// if (isOpX<ASSUMPTION> (e)){
	//   const Assumption np = dynamic_cast<const ASSUMPTION&>(e->op()).get();
	//   std::string svar = "ASM(" + boost::lexical_cast<std::string>(np.p.first) + "," + 
	//                              boost::lexical_cast<std::string>(np.p.second) + ")";
          
	//   Z3_ast res = 
	//       Z3_mk_const (ctx, 
	// 		   Z3_mk_string_symbol (ctx, svar.c_str ()),
	// 		   Z3_mk_bool_sort (ctx));


	//   eMap[e] = res;
	//   zMap[Z3_get_ast_id(ctx, res)] = e;
	//   return res;
	// }
	

	return EC::marshal (ctx, e, eMap, zMap);
      }
    
      template <typename zmap_type>
      static Expr unmarshal (Z3_context ctx,
	                     Z3_ast z, 
			     ExprFactory &efac,
			     zmap_type &zMap)
      {
	std::map<unsigned,Expr>::const_iterator it = 
	  zMap.find (Z3_get_ast_id (ctx, z));
	if (it != zMap.end ()) return it->second;

	return EC::unmarshal (ctx, z, efac, zMap);
      }
    
    };
  

    /** Basic Boolean and Numeric expressions*/
    template <typename EC> 
    struct BasicExprConverter
    {
      template <typename emap_type, typename zmap_type>
      static Z3_ast marshal (Z3_context ctx, 
			     Expr e,
			     emap_type &eMap,
			     zmap_type &zMap)
      {
	assert(e);
	if (isOpX<TRUE>(e)) return Z3_mk_true (ctx);
	if (isOpX<FALSE>(e)) return Z3_mk_false (ctx);

	// -- see if we've done this before
	typedef typename emap_type::iterator emap_iterator;
	emap_iterator it = eMap.find (e);
	if (it != eMap.end ()) return it->second;

	Z3_ast res = NULL;

	if (isOpX<INT>(e))
	  res = 
	    Z3_mk_numeral(ctx, 
			  boost::lexical_cast<std::string>(e.get()).c_str(),
			  ufocl::USE_INTS ? Z3_mk_int_sort (ctx) : 
			  Z3_mk_real_sort (ctx));

	else if (isOpX<MPQ>(e))
	  {
	    const MPQ& op = dynamic_cast<const MPQ&>(e->op ());
	    res = 
	      Z3_mk_numeral
	      (ctx, 
	       boost::lexical_cast<std::string> (op.get()).c_str (),
	       ufocl::USE_INTS ? Z3_mk_int_sort (ctx) : Z3_mk_real_sort (ctx));
	  }
	else if (isOpX<MPZ>(e))
	  {
	    const MPZ& op = dynamic_cast<const MPZ&>(e->op ());
	    res = 
	      Z3_mk_numeral
	      (ctx, 
	       boost::lexical_cast<std::string> (op.get()).c_str (),
	       Z3_mk_int_sort (ctx));
	  }
	else if (bind::isBoolVar (e))
	  {
	    // XXX The name 'edge' is misleading. Should be changed.
	    Expr edge = bind::name (e);    
	    string svar;
	    // -- for variables with string names, use the names
	    if (isOpX<STRING> (edge))
	      svar = getTerm<std::string> (edge);
	    else // -- for non-string named variables use address
	      svar = "E" + 
		boost::lexical_cast<std::string,void*> (edge.get());

	    Z3_sort sort = Z3_mk_bool_sort (ctx);
	    res = Z3_mk_const (ctx, 
			       Z3_mk_string_symbol (ctx, svar.c_str ()),
			       sort);
	  }
	else if (bind::isIntVar (e))
	  {
	    Expr name = bind::name (e);
	    string sname;
	    if (isOpX<STRING> (name)) 
	      sname = getTerm<std::string> (name);
	    else 
	      sname = "I" + lexical_cast<string,void*> (name.get ());

	    Z3_sort sort = Z3_mk_int_sort (ctx);
	    res = Z3_mk_const (ctx, 
			       Z3_mk_string_symbol (ctx, sname.c_str ()),
			       sort);
	  }
	else if (bind::isRealVar (e))
	  {
	    Expr name = bind::name (e);
	    string sname;
	    if (isOpX<STRING> (name))
	      sname = getTerm<string> (name);
	    else
	      sname = "R" + lexical_cast<string,void*> (name.get ());
	    
	    Z3_sort sort = Z3_mk_real_sort (ctx);
	    res = Z3_mk_const (ctx,
			       Z3_mk_string_symbol (ctx, sname.c_str ()),
			       sort);
	  }
	


	if (res)
	  {
	    // -- store numbers to simplify marshal/unmarshal of them.
	    eMap[e] = res;
	    zMap[Z3_get_ast_id(ctx, res)] = e;
	    return res;
	  }

      
	int arity = e->arity ();
	/** other terminal expressions */
	if (arity == 0) return EC::marshal (ctx, e, eMap, zMap);
	
      
	if (arity == 1)
	  {
	    //then it's a NEG or UN_MINUS or ABS
	    //XXX not sure how to handle ABS 
	    if (isOpX<UN_MINUS>(e))
	      return Z3_mk_unary_minus(ctx,
				       marshal (ctx, e->left(), eMap, zMap));
	    if (isOpX<NEG>(e))
	      return Z3_mk_not(ctx, marshal (ctx, e->left(), eMap, zMap));
	  
	    return EC::marshal (ctx, e, eMap, zMap);
	  }
	else if (arity == 2)
	  {

	    Z3_ast t1 = marshal(ctx, e->left(), eMap, zMap);
	    Z3_ast t2 = marshal(ctx, e->right(), eMap, zMap);

	    Z3_ast args [2];
	    
	    /** BoolOp */
	    if (isOpX<AND>(e)) 
	      {
		args [0] = t1;
		args [1] = t2;
		res = Z3_mk_and(ctx, 2, args);
	      }
	    else if (isOpX<OR>(e))
	      {
		args [0] = t1;
		args [1] = t2;
		res = Z3_mk_or(ctx, 2, args);
	      }
	    else if (isOpX<IMPL>(e))
	      res = Z3_mk_implies(ctx,t1, t2);
	    else if (isOpX<IFF>(e))
	      res = Z3_mk_iff(ctx, t1, t2);
	    else if (isOpX<XOR>(e))
	      res = Z3_mk_xor(ctx, t1, t2);

	    /** NumericOp */
	    else if (isOpX<PLUS>(e))
	      {
		args [0] = t1;
		args [1] = t2;
		res = Z3_mk_add(ctx, 2, args);
	      }
	    else if (isOpX<MINUS>(e))
	      {
		args [0] = t1;
		args [1] = t2;
		res = Z3_mk_sub(ctx, 2, args);
	      }
	    else if (isOpX<MULT>(e))
	      {
		args [0] = t1;
		args [1] = t2;
		res = Z3_mk_mul(ctx, 2, args);
	      }
	    else if (isOpX<DIV>(e))
	      res = Z3_mk_div (ctx, t1, t2);

	    /** Comparisson Op */
	    else if (isOpX<EQ>(e))
	      res = Z3_mk_eq (ctx, t1, t2);
	    else if (isOpX<NEQ>(e))
	      res = Z3_mk_not (ctx, Z3_mk_eq (ctx, t1, t2));
	    else if (isOpX<LEQ>(e))
	      res =  Z3_mk_le(ctx, t1, t2);
	    else if (isOpX<GEQ>(e))
	      res = Z3_mk_ge(ctx, t1, t2);
	    else if (isOpX<LT>(e))
	      res = Z3_mk_lt(ctx, t1, t2);
	    else if (isOpX<GT>(e))
	      res = Z3_mk_gt(ctx, t1, t2);
	    
	    else
	      return EC::marshal (ctx, e, eMap, zMap);
	  }
	else if (isOpX<AND> (e) || isOpX<OR> (e) ||
		 isOpX<ITE> (e) || isOpX<XOR> (e) ||
		 isOpX<PLUS> (e) || isOpX<MINUS> (e) ||
		 isOpX<MULT> (e))
	  {
	    std::vector<Z3_ast> args;
      
	    for (ENode::args_iterator it = e->args_begin(), end = e->args_end();
		 it != end; ++it)
	      args.push_back (marshal (ctx, *it, eMap, zMap));
          
	    if (isOp<ITE>(e))
	      {
		assert (e->arity () == 3);
		res = Z3_mk_ite(ctx,args[0],args[1],args[2]);
	      }
	    else if (isOp<AND>(e))
	      {
		Z3_ast* z3args = new Z3_ast [args.size ()];
		assert (z3args != NULL);

		for (size_t i = 0; i < args.size (); i++)
		  z3args [i] = args [i];
		res = Z3_mk_and (ctx, args.size (), z3args);
		delete [] z3args;
	      }
	    else if (isOp<OR>(e))
	      {
		Z3_ast* z3args = new Z3_ast [args.size ()];
		assert (z3args != NULL);

		for (size_t i = 0; i < args.size (); i++)
		  z3args [i] = args [i];
		res = Z3_mk_or (ctx, args.size (), z3args);
		delete [] z3args;
	      }
	    else if (isOp<XOR>(e))
	      {
		res = args[0];
		for (size_t i = 1; i < args.size(); ++i)
		  res = Z3_mk_xor (ctx, res, args[i]);
	      }
	    else if (isOp<PLUS>(e))
	      {
		Z3_ast* z3args = new Z3_ast [args.size ()];
		assert (z3args != NULL);

		for (size_t i = 0; i < args.size (); i++)
		  z3args [i] = args [i];
		res = Z3_mk_add (ctx, args.size (), z3args);
		delete [] z3args;
	      }
	    else if (isOp<MINUS>(e))
	      {
		Z3_ast* z3args = new Z3_ast [args.size ()];
		assert (z3args != NULL);

		for (size_t i = 0; i < args.size (); i++)
		  z3args [i] = args [i];
		res = Z3_mk_sub (ctx, args.size (), z3args);
		delete [] z3args;
	      }
	    else if (isOp<MULT>(e))
	      {
		Z3_ast* z3args = new Z3_ast [args.size ()];
		assert (z3args != NULL);

		for (size_t i = 0; i < args.size (); i++)
		  z3args [i] = args [i];
		res = Z3_mk_mul (ctx, args.size (), z3args);
		delete [] z3args;
	      }

	  }
	  else
	    return EC::marshal (ctx, e, eMap, zMap);
      
	assert (res != NULL);
	eMap [e] = res;
	//mMap [msat_term_id (res)] = e;
      
	return res;
      }

      template <typename zmap_type>    
      static Expr unmarshal (Z3_context ctx,
	                     Z3_ast z, 
			     ExprFactory &efac,
			     zmap_type &zMap)
      {
	Z3_lbool bVal = Z3_get_bool_value (ctx, z);
	if (bVal == Z3_L_TRUE) return mk<TRUE> (efac);
	if (bVal == Z3_L_FALSE) return mk<FALSE> (efac);

	Z3_ast_kind kind = Z3_get_ast_kind (ctx, z);
	


	if (kind == Z3_NUMERAL_AST)
	  {
	    std::string snum = Z3_get_numeral_string (ctx, z);
	    const mpq_class mpq(snum);
	    return mkTerm (mpq, efac);
	  }

	Z3_app app = Z3_to_app (ctx, z);
	Z3_func_decl fdecl = Z3_get_app_decl (ctx, app);
	Z3_decl_kind dkind = Z3_get_decl_kind (ctx, fdecl);
      
	if (dkind == Z3_OP_NOT)
	  {
	    assert (Z3_get_app_num_args (ctx, app) == 1);
	    return mk<NEG> (unmarshal 
			    (ctx, Z3_get_app_arg (ctx, app, 0), efac, zMap));
    	}
	if (dkind == Z3_OP_UMINUS)
	  return mk<UN_MINUS> (unmarshal (ctx, Z3_get_app_arg (ctx, app, 0),
					  efac, zMap));

      
	typedef typename zmap_type::const_iterator zmap_const_iterator;
	zmap_const_iterator it = zMap.find (Z3_get_ast_id (ctx, z));
	if (it != zMap.end ()) return it->second;


	/** newly introduced Z3 symbol (i.e., variable) */
	if (dkind == Z3_OP_UNINTERPRETED)
	  {
	    Expr type;
	    Expr name;

	    Z3_symbol symname = Z3_get_decl_name (ctx, fdecl);
	    assert (Z3_get_symbol_kind (ctx, symname) == Z3_STRING_SYMBOL);
	    std::string sname = 
	      std::string (Z3_get_symbol_string (ctx, symname));
	    name = mkTerm (sname, efac);
	    
	    Z3_sort sort = Z3_get_range (ctx, fdecl);
	    switch (Z3_get_sort_kind (ctx, sort))
	      {
	      case Z3_BOOL_SORT: 
		type = mk<BOOL_TY> (efac);
		break;
	      case Z3_INT_SORT:
		type = mk<INT_TY> (efac);
		break;
	      case Z3_REAL_SORT:
		type = mk<REAL_TY> (efac);
		break;
	      default:
		assert (0 && "Unsupported SORT");
	      }
	    
	    Expr res = bind::var (name, type);
	    zMap [Z3_get_ast_id (ctx, z)] = res;
	    
	    return res;
	  }
	

	Expr e;
	std::vector<Expr> args;
	for (size_t i = 0; i < (size_t)Z3_get_app_num_args (ctx, app); i++)
	  args.push_back (unmarshal 
			  (ctx, Z3_get_app_arg(ctx, app, i), efac, zMap));
	
	switch (dkind)
	  {
	  case Z3_OP_ITE:
	    e = mknary<ITE> (args.begin (), args.end ());
	    break;
	  case Z3_OP_AND:
    	    e = mknary<AND> (args.begin(), args.end());
    	    break;
	  case Z3_OP_OR:
	    e =  mknary<OR> (args.begin(), args.end());
	    break;
	  case Z3_OP_XOR:
	    e = mknary<XOR> (args.begin(), args.end());
	    break;
	  case Z3_OP_IFF:
	    e =  mknary<IFF> (args.begin(), args.end());
	    break;
	  case Z3_OP_IMPLIES:
	    e =  mknary<IMPL> (args.begin(), args.end());
	    break;
	  case Z3_OP_EQ:
	    e =  mknary<EQ> (args.begin(), args.end());
	    break;
	  case Z3_OP_LT:
	    e =  mknary<LT> (args.begin(), args.end());
	    break;
	  case Z3_OP_GT:
	    e =  mknary<GT> (args.begin(), args.end());
	    break;
	  case Z3_OP_LE:
	    e =  mknary<LEQ> (args.begin(), args.end());
	    break;
	  case Z3_OP_GE:
	    e =  mknary<GEQ> (args.begin(), args.end());
	    break;
	  case Z3_OP_ADD:
	    e =  mknary<PLUS> (args.begin(), args.end());
	    break;
	  case Z3_OP_SUB:
	    e =  mknary<MINUS> (args.begin(), args.end());
	    break;
	  case Z3_OP_MUL:
	    e =  mknary<MULT> (args.begin(), args.end());
	    break;
	  case Z3_OP_DIV:
	    e = mknary<DIV> (args.begin(), args.end());
	    break;
	  case Z3_OP_MOD:
	    e = mknary<MOD> (args.begin(), args.end());
	    break;
	  default:
	    return EC::unmarshal (ctx, z, efac, zMap);
	  }

	
	zMap [Z3_get_ast_id (ctx, z)] = e;
	return e;
      }		       
    };

    typedef TerminalUfo<TerminalAssert> UFO_TERMINAL;
    typedef BasicExprConverter<UFO_TERMINAL> ExprConverter;
  }
}

namespace ufo 
{
  typedef Z3<Expr,z3::ExprConverter> ExprZ3;

  /**
   * Simplify an expression using Z3 strong context simplifier
   *
   * \param[in] e an expression to be simplified
   * \return an equivalent expression after simplification
   */
  inline Expr z3_simplify (Expr e)
  {
    StringMap opt;
    opt ["STRONG_CONTEXT_SIMPLIFIER"] = "true";
    opt ["ELIM_QUANTIFIERS"] =  "false";
    opt ["ELIM_AND"] =  "false";
    ExprZ3 z3 (e->efac (), opt);
    return z3.simplify (e);
  }

  /**
   * Simplify an expression using Z3 default simplifier
   *
   * \param[in] e an expression to be simplified
   * \return an equivalent expression after simplification
   */
  inline Expr z3_lite_simplify (Expr e)
  {
    StringMap opt;
    opt ["STRONG_CONTEXT_SIMPLIFIER"] = "false";
    opt ["ELIM_QUANTIFIERS"] =  "false";
    opt ["ELIM_AND"] =  "false";
    ExprZ3 z3 (e->efac (), opt);
    return z3.simplify (e);
  }
  
  inline Expr z3_forall_elim (Expr e, const ExprSet &vars)
  {
    StringMap opt;
    opt ["STRONG_CONTEXT_SIMPLIFIER"] = "false";
    opt ["ELIM_QUANTIFIERS"] =  "true";
    opt ["ELIM_AND"] =  "false";
    ExprZ3 z3 (e->efac (), opt);
    return z3.forallElim (e, vars);
  }

  inline boost::tribool z3_is_sat (Expr e)
  {
    ExprZ3 z3 (e->efac ());
    z3.assertExpr (e);
    return z3.solve ();
  }
  
  template <typename Range>  
  boost::tribool z3_is_sat_assuming (Expr e, const Range &assumptions, 
				     ExprSet &result)
  {
    ExprZ3 z3 (e->efac ());
    z3.assertExpr (e);
    
    return z3.solveAssuming (assumptions, 
			     std::inserter (result, result.begin ()));
  }

  inline Expr z3_all_sat (Expr e, const ExprVector &terms)
  {
    ExprZ3 z3 (e->efac (), StringMap(), true);
    return z3.allSAT (e, terms);
  }
  

  inline Expr z3_from_smtlib2 (ExprFactory &efac, std::string smt)
  {
    ExprZ3 z3 (efac);
    return z3.fromSmtLib2Str (smt);
  }

  inline std::string z3_to_smtlib2_with_decl (Expr f)
  {
    ExprZ3 z3 (f->efac ());
    return z3.toSmtLib2DeclStr (f) + "\n" + 
      "(assert " + z3.toSmtLib2Str (f) + ")";
  }
  
  inline std::string z3_to_smtlib2 (Expr f)
  {
    ExprZ3 z3 (f->efac ());
    return z3.toSmtLib2Str (f);
  }
  


}



#endif
