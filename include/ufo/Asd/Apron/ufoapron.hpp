#ifndef __UFO_APRON_HPP_
#define __UFO_APRON_HPP_

#include "ap_global0.h"

#include "box.h"
#include "pk.h"
#include "oct.h"
#include "pkeq.h"
#include "ufo/Cpg/Lbe.hpp"
#include "ufo/Asd/AbstractStateDomain.hpp"
#include "ufo/Asd/BoolAbstractStateDomain.hpp"

#include "Apron.hpp"

using namespace std;

namespace ufoapron
{  
  typedef ap_state_ptr ap_ptr;

  /**
   * Apron Abstract State Value
   */
  class ApronAbstractStateValue : public AbstractStateValue
  {
  private:
    ap_ptr val;
  public:
    ApronAbstractStateValue (ap_ptr _s) : val (_s) {}
    ap_ptr getVal () { return val; }
  };
    
  /** parameterized by type T of expressions and a converter EC
      between apron abstract states and T */
  template <typename T, typename EC> 
  class APRON : public AbstractStateDomain
  {
  private:
    typedef APRON<T,EC> this_type;

    // map from Expr to ap_state
    typedef boost::unordered_map<Expr, ap_ptr> expr_map_type;
    expr_map_type exprMap;    
    // map from ap_state to Expr
    typedef boost::unordered_map<ap_ptr, Expr> ap_map_type;
    ap_map_type apMap;
    // map from Expr to dimension id
    map<Expr, unsigned int> var2dim;
    ExprVector dim2var;

    ExprFactory& efac;
    const ExprSet &vars;
    size_t dimSize;    
    ap_manager_t* man;

  public:
    APRON (ExprFactory &_efac, const ExprSet &_dims, ap_manager_t* _m) : 
      efac(_efac), vars(_dims), dimSize(vars.size()), man (_m)
    {
      unsigned int i = 0;
      foreach (Expr _v, vars)
      {
        var2dim[_v] = i++;
        dim2var.push_back (_v);
      }
    }

    ~APRON () 
    {
      ap_manager_free (man);
    }

    virtual std::string name () { return "APRON"; }
    
    AbstractState abs (ap_ptr _s)
    {
      return AbstractState (new ApronAbstractStateValue (_s));
    }

    const ExprSet& getVars () { return vars; }

    ap_ptr getVal (AbstractState _a)
    {
      return dynamic_cast<ApronAbstractStateValue*>(&*_a)->getVal();
    }

    virtual AbstractState alpha (CutPointPtr loc, Expr val)
    {
      return abs (expr2ap (val));
    }

    virtual Expr gamma (CutPointPtr loc, AbstractState v)
    {
      return gamma (getVal(v));
    }
    
    AbstractState top (CutPointPtr loc)
    {
      return abs(top ());
    }

    AbstractState bot (CutPointPtr loc)
    {
      return abs(bot ());
    }

    bool isTop (CutPointPtr loc, AbstractState v)
    {
      return ap_abstract0_is_top (man, &*getVal(v));
    }

    bool isBot (CutPointPtr loc, AbstractState v)
    {
      return ap_abstract0_is_bottom (man, &*getVal(v));
    }

    bool isLeq (CutPointPtr loc, AbstractState v1,
                AbstractState v2)
    {
      return ap_abstract0_is_leq (man, &*getVal(v1), &*getVal(v2));
    }

    bool isLeq (CutPointPtr loc, AbstractState v1,
                AbstractStateVector const &u)
    {
      assert (0 && "Unimplemented");
      return false;
    }

    AbstractState post (AbstractState pre,
                        CutPointPtr src, CutPointPtr dst)
    {
      assert (0 && "Unimplemented");
      return abs (apPtr(man,NULL));
    }

    void refinementHint (CutPointPtr loc, Expr hint)
    {
      assert (0 && "Unimplemented");
    }

    bool isFinite ()
    {
      return false;
    }

    AbstractState post (const LocLabelStatePairVector &pre,
                        Loc dst,
                        BoolVector &deadLocs)
    {
      assert (0 && "Unimplemented");
      return abs(apPtr(man, NULL));
    }

    bool isEq (CutPointPtr loc, AbstractState v1,
               AbstractState v2)
    {
      return ap_abstract0_is_eq (man, &*getVal(v1), &*getVal(v2));
    }
    
    AbstractState join (CutPointPtr loc, 
                        AbstractState v1, AbstractState v2)
    {
      return abs (join (getVal(v1), getVal(v2)));
    }

    AbstractState meet (CutPointPtr loc, 
                        AbstractState v1, AbstractState v2)
    {
      return abs (meet (getVal(v1), getVal(v2)));
    }

    AbstractState widen (CutPointPtr loc, 
                         AbstractState v1, AbstractState v2)
    {
      return abs (widen (getVal(v1), getVal(v2)));
    }

    AbstractState widenWith (Loc loc, AbstractStateVector const &oldV,
                             AbstractState newV)
    {
      assert (0 && "Unimplemented");
      return abs(apPtr(man, NULL));
    }

    AbstractState copy (AbstractState _x)
    {
      return abs (copy (getVal(_x)));
    }

    /**
     * apron bottom
     * return a bottom state
     */
    ap_ptr bot ()
    {
      return apPtr(man, ap_abstract0_bottom (man, 0, dimSize));
    }

    /**
     * apron top
     * return a top state
     */
    ap_ptr top ()
    {
      return apPtr(man, ap_abstract0_top (man, 0, dimSize));
    }

    /**
     * apron join
     * param[in] ap_state _x
     * param[in] ap_state _y
     * return ap_state: joined state
     */
    ap_ptr join (ap_ptr _x, ap_ptr _y)
    {
      return apPtr(man, ap_abstract0_join (man, false, &*_x, &*_y));
    }

    /**
     * apron meet
     * param[in] ap_state _x
     * param[in] ap_state _y
     * return ap_state
     */
    ap_ptr meet (ap_ptr _x, ap_ptr _y)
    {
      return apPtr(man, ap_abstract0_meet (man, false, &*_x, &*_y));
    }

    /**
     * apron widen
     * param[in] ap_state _x
     * param[in] ap_state _y
     * return ap_state: widened state of _x against _y
     */
    ap_ptr widen (ap_ptr _x, ap_ptr _y)
    {
      //assert (ap_abstract0_is_leq (man, &*_x, &*_y));
      return apPtr(man, ap_abstract0_widening (man, &*_x, &*_y));
    }

    /**
     * make a copy of state
     * param[in] ap_state _x
     * return a copy of _x
     */
    ap_ptr copy (ap_ptr _x)
    {
      return apPtr(man, ap_abstract0_copy (man, &*_x));
    }

    /**
     * Convert a collection of linear constraints to apron state
     * param[in] Range _p.
     * return an abstract state
     */
    template <typename R>
    ap_ptr cons2state (R &_p)
    {
      return apPtr(man, (EC::marshal_cons (man, _p, var2dim)));
    }
    
    /**
     * Convert a model (point) to an apron state
     * param[in] Range p. Expr representations of predefined dimensions
     * return an abstract state approximates the model
     */
    template <typename R>
    AbstractState point2state (R &_p)
    {
      //typename range_difference<R>::type dimSize = size(_p);
      ap_tcons0_array_t p_array = ap_tcons0_array_make (dimSize);
      ap_tcons0_t cons;

      typedef pair<Expr,Expr> ExprPair;
      unsigned int i = 0;
      foreach (ExprPair _v, _p)
      {
        ap_texpr0_t* ap_v = ap_texpr0_dim (var2dim[_v.first]);
          
        if (isOpX<MPQ> (_v.second))
        {
          // v - x == 0
          ap_texpr0_t* ap_vx = ap_texpr0_binop 
            (AP_TEXPR_SUB, ap_v, 
             ap_texpr0_cst_scalar_mpq (getTerm<mpq_class>(_v.second).get_mpq_t()),
             AP_RTYPE_REAL, AP_RDIR_NEAREST);
          cons = ap_tcons0_make (AP_CONS_EQ, ap_vx, NULL);
        }
        else
        {
          return abs (apPtr(man, ap_abstract0_top (man, 0, dimSize)));
        }

        p_array.p[i++] = cons;
      }
      
      ap_state p_state = ap_abstract0_of_tcons_array (man, 0, dimSize, &p_array);
      ap_tcons0_array_clear (&p_array);
     
      return abs(apPtr(man, p_state));
    }
        
    /**
     * gamma function, converts abstract state to Expr
     * based on lincons representation.
     * param[in] ap_state a
     * return Expr. Representation of the state a
     */
    Expr gamma (ap_ptr a)
    {
      return EC::unmarshal (man, &*a, efac, apMap, dim2var); 
    }

    /**
     * Over-approximation gamma function.
     * Based on the bounds of each dimension
     */
    Expr gamma_bound (ap_ptr a)
    {
      ExprVector g;
      foreach (Expr _v, vars)
      {
        g.push_back (getBound (_v, *a).gamma(_v));
      }
      return mknary<AND> (mk<TRUE>(efac), g.begin(), g.end());
    }

    /**
     * Get bounds (interval) of a certain predefined dimension
     * param[in] Expr _name, dimension name
     * param[in] ap_state _s, abstract state
     * return Interval representation of the dimension
     */
    Interval getBound (Expr _name, AbstractState _s)
    {
      assert (var2dim.count(_name) > 0);
      ap_ptr s = getVal (_s);
      return getBound (_name, s);
    }

    Interval getBound (Expr _name, ap_ptr _s)
    {
      assert (var2dim.count(_name) > 0);
      ap_interval_t* res = ap_abstract0_bound_dimension
        (man, &*_s, dim(_name));
      return ap_itv2itv (res);
    }

    /**
     * expr2ap.
     * param[in] Expr e: an expression with linear constraints.
     * The input has to comply with the following restrictions:
     * 1. No Boolean variables. Boolean variables are supposed to be handled by
     * "BoolAbstractStateDomain", and together form a product state.
     * (This is yet to be implemented.)
     * 2. Formula with relatively small size.
     * (Not fully tested on very large formula.)
     * return ap_state: an apron abstract state.
     */
    ap_ptr expr2ap (Expr e)
    {
      return apPtr(man, EC::marshal (man, e, exprMap, apMap, var2dim));
    }

    ap_ptr consarray2ap (ExprVector &e)
    {
      return apPtr(man, EC::marshal_cons (man, e, var2dim));
    }

    /**
     * ap2expr.
     * param[in] ap_state a: apron abstract state
     * return Expr: Expr representation of the constraints stored in the state.
     */
    Expr ap2expr (ap_ptr a)
    {
      assert (a); // assert state is not NULL
      return EC::unmarshal (man, &*a, efac, apMap, dim2var);
    }

    /**
     * Some printing functions
     */
    void print (ap_state a)
    {
      ap_abstract0_fprint (stderr, man, a, NULL);
    }

    void print (ap_ptr a)
    {
      print (*a);
    }

    void print (ap_tcons0_t t)
    {
      ap_tcons0_fprint (stderr, &t, NULL);
    }

    void print (AbstractState a)
    {
      errs () << "Apron State:\n";
      print (getVal (a));
    }

  private:
    /**
     * dim.
     * param[in] Expr representation of variable
     * return dimension id in apron
     */
    unsigned int dim (Expr _v)
    {
      assert (var2dim.count (_v) > 0);
      return var2dim[_v];
    }

    /**
     * var.
     * param[in] dimension id
     * return Expr representation of variable
     */
    Expr var (unsigned int _dim)
    {
      assert (_dim < dimSize);
      return dim2var[_dim];
    }

    /**
     * apron interval to ufo interval
     * param[in] ap_interval_t* _itv, apron interval
     * return Interval, ufo interval
     */
    Interval ap_itv2itv (ap_interval_t* _itv)
    {
      if (_itv->inf->discr == AP_SCALAR_DOUBLE)
        return Interval (mpq_class (_itv->inf->val.dbl), 
                         mpq_class (_itv->sup->val.dbl));

      if (_itv->inf->discr == AP_SCALAR_MPQ)
        return Interval (mpq_class (_itv->inf->val.mpq), 
                         mpq_class (_itv->sup->val.mpq));

      else assert (0 && "Unreachable");
    }
  };

  /**
   * BoxApron.
   * Apron abstract domain using Box library.
   */
  template <typename T, typename EC>
  class BoxApron : public APRON<T,EC>
  {
  public:
    BoxApron (ExprFactory &_efac, const ExprSet &_dims) : 
      APRON<T,EC> (_efac, _dims, box_manager_alloc()) {}

    std::string name () {return "Apron Box Domain";}
  };

  /**
   * OctApron.
   * Apron abstract domain using octagon library.
   */
  template <typename T, typename EC>
  class OctApron : public APRON<T,EC>
  {
  public:
    OctApron (ExprFactory &_efac, const ExprSet &_dims) : 
      APRON<T,EC> (_efac, _dims, oct_manager_alloc()) {}

    std::string name () {return "Apron Octagon Domain";}
  };

  /**
   * PkApron.
   * Apron abstract domain using NewPolka library.
   */
  template <typename T, typename EC>
  class PkApron : public APRON<T,EC>
  {
  public:
    PkApron (ExprFactory &_efac, const ExprSet &_dims) : 
      APRON<T,EC> (_efac, _dims, pk_manager_alloc(false)) {}

    std::string name () {return "Apron NewPolka Domain";}
  };
    
  /**
   * BoolApronProdcut.
   * Product abstract domain of Boolean and APRON
   */
  template <typename AP>
  class BoolApronProduct : public ProductAbstractStateDomain
  {
  private:
    BoolAbstractStateDomain babs;
    AP pabs;
  public:
    BoolApronProduct (ExprFactory &_e, ExprSet &_b, ExprSet &_r) :
      ProductAbstractStateDomain (babs,pabs),
      babs(BoolAbstractStateDomain (_e,_b)),
      pabs(AP (_e,_r)) {}
  
    ~BoolApronProduct () {}
    
    /**
     * point2state.
     * param[in] R bm: Boolean map from var->val representing the Boolean
     * portion of the point
     * param[in] R rm: real map from var->val representing the real portion of
     * the point
     * return abstract state
     */
    template <typename R>
    AbstractState point2state (R &bm, R &rm)
    {
      return absState (babs.point2state (bm), pabs.point2state (rm));
    }
    
    /**
     * getBound.
     * param[in] Expr _x: variable name
     * param[in] AbstractState _s: state
     * return Interval bound
     */
    Interval getBound (Expr _x, AbstractState _s)
    {
      AbsStatePair pair = getAbsVal (_s);
      if (babs.getVars().count (_x) > 0)
        return babs.getBound (_x, pair.first);
      else if (pabs.getVars().count (_x) > 0)
        return pabs.getBound (_x, pair.second);
      else assert (0 && "unreachable");
    }

    /**
     * copy.
     * return deep copy of state
     */
    AbstractState copy (AbstractState _x)
    {
      AbsStatePair pair = getAbsVal (_x);
      return absState (babs.copy(pair.first), pabs.copy(pair.second));
    }

    /**
     * print.
     * Print out the state nicely.
    */
    void print (AbstractState _s)
    {
      AbsStatePair pair = getAbsVal (_s);
      babs.print (pair.first);
      pabs.print (pair.second);
    }
  };

  /**
   * BoolBoxProduct.
   * Product of Boolean abstract domain and Box domain
   */
  template <typename T, typename EC>
  class BoolBoxProduct : public BoolApronProduct<BoxApron<T,EC> >
  {
  public:  
    BoolBoxProduct (ExprFactory &_e, ExprSet &_b, ExprSet &_r) :
      BoolApronProduct<BoxApron<T,EC> > (_e, _b, _r) {}
  };

  /**
   * BoolOctProduct.
   * Product of Boolean abstract domain and Octagon domain
   */
  template <typename T, typename EC>
  class BoolOctProduct : public BoolApronProduct<OctApron<T,EC> >
  {
  public:  
    BoolOctProduct (ExprFactory &_e, ExprSet &_b, ExprSet &_r) :
      BoolApronProduct<OctApron<T,EC> > (_e, _b, _r) {}
  };

  /**
   * BoolPkProduct.
   * Product of Boolean abstract domain and NewPolka domain
   */
  template <typename T, typename EC>
  class BoolPkProduct : public BoolApronProduct<PkApron<T,EC> >
  {
  public:  
    BoolPkProduct (ExprFactory &_e, ExprSet &_b, ExprSet &_r) :
      BoolApronProduct<PkApron<T,EC> > (_e, _b, _r) {}
  };
}

#endif
