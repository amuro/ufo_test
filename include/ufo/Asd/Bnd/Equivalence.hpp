#ifndef __EQUIVALENCE_H_
#define __EQUIVALENCE_H_

#include "Disjoint.hpp" 

namespace ufo
{

  template <typename T, typename EqualTo = std::equal_to<T> >
  class Equivalence : public DisjointSets<T>
  {
    public:
      Equivalence () {}

      Equivalence (std::set<T> elems) : DisjointSets<T, EqualTo>(elems) {}  

      /**
       * Relate element x and y
       */
      void relate (T x, T y)
      {
        union_set (x, y);
      }

      /**
       * Test whether x and y are related
       */
      bool isRelated (T x, T y)
      {
        return EqualTo() (find_parent (x), find_parent (y));
      }

      /**
       * Equivalence class containing element x
       */
      std::set<T> equiClass (T x)
      {
        return get_set (x);
      }
  };
}

#endif
