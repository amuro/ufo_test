#ifndef __COI_CACHE_H_
#define __COI_CACHE_H_

namespace ufo
{
  /**
   * Cache of the "Cone of Influence".
   */
  template <typename T, typename K, size_t cap = 1000>
    class COICache
    {
      private:
        typedef std::map< std::pair<T, T>, K > CacheMap;
        CacheMap cache;

      public:
        COICache () {}

        /** Write to cache */
        void write (T var, T phi, K bound)
        {
          std::pair<T, T> key = make_pair(var, phi);
          if (cache.find (key) != cache.end())
            return;
          if (cache.size() >= cap)
            cache.erase(cache.begin());

          cache[key] = bound;
        }

        bool isHit (T var, T phi)
        {
          return cache.find(make_pair(var, phi)) != cache.end();
        }

        /** Read from cache. Assume there is a cache hit */
        K read (T var, T phi)
        {
          assert (isHit (var, phi));
          return cache[make_pair(var, phi)];
        }

        /** Current cache size */
        int size ()
        {
          return cache.size();
        }

      //private:
        /*  Compute the cone of influence.
         *  Precond: var is in formula and scanCons is called.
         *  Return expr C, where C is the smallest conjunct containning var
         *  in the formula, such that the other parts of the formula
         *  is not "related" to var.
         */
        /*
        Expr computeCOI (Expr formula, Expr var)
        {
          if (formulaSAT == UNSAT)
            return mk<FALSE>(efac);

          if (!isOpX<AND>(formula))
            return formula;

          Expr res = mk<TRUE>(efac);
          for (ENode::args_iterator b = formula->args_begin (), 
              e = formula->args_end (); 
              b != e; ++b)
          {
            ExprSet vars = collectVars(*b);
            // Conjunct contains var.
            // Recursively look for smaller COI.
            if (vars.find(var) != vars.end())
              res = boolop::land(res, computeCOI (*b, var));
            // Conjunct does not contain var.
            else
            {
              foreach (Expr nv, vars)
              {
                // Add conjunct to COI if any variable in it is related
                // to var.
                if (variables.isRelated(nv, var))
                {
                  res = boolop::land(res, *b);
                  break;
                }
              }
            }
          }
          return res;
        }
      */  
    };
}

#endif
