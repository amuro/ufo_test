#include "ufo/Smt/Z3.hpp"

namespace ufoz3
{
  void error_handler (Z3_error_code e)
  {
    errs () << "Z3 Error: "
	    << Z3_get_error_msg (e)
	    << "\n";
    errs ().flush ();
    exit (1);
  }
}

