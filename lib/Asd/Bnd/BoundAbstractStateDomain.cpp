#include "ufo/Asd/Bnd/BoundAbstractStateDomain.hpp"

#include "ufo/Cpg/Lbe.hpp"
#include "ufo/EdgeComp.hpp"

#include "ufo/Smt/ExprMSat.hpp"
#include "ufo/Smt/ExprZ3.hpp"
#include "ufo/Asd/Apron/ExprApron.hpp"

#include "ufo/Asd/Bnd/VarBound.hpp"
#include "ufo/Asd/Bnd/BoxBound.hpp"
#include "ufo/Asd/Bnd/DNFBound.hpp"

#include "ufo/Stats.hpp"
#include "ufo/Asd/Bnd/COI.hpp"


namespace ufo
{
  
  
  namespace
  {
    
    struct VarsFilter
    {
      bool operator() (Expr v) const { return isOpX<VALUE> (v); }
    };

    struct TermsFilter
    {
      bool operator() (Expr v) const 
      { return isOp<NumericOp> (v) || isOpX<VALUE> (v); }
    };
  }


  Interval &BoundAbstractStateDomain::getBounds (AbstractState v, 
						 Expr var) const 
  {
    assert (!isTopV(v));
    assert (!isBotV(v));
    assert (v && "v is NULL");

    VarBounds vbs = dynamic_cast<BoundAbstractStateValue*>(&*v)->getVal();

    if (vbs.count(var) <= 0)
      {
	errs() << "Not Found!" << "\n";
	errs() << "Var: " << *var << "\n";
	forall (ExpIntPair vb, vbs)
          {
            errs() << *vb.first << " \n";
            print (vb.second);
          }
      }


    Interval& bounds =  dynamic_cast<BoundAbstractStateValue*>(&*v)->
      getVal ().find(var)->second;

    assert (!isWrong(bounds.first));
    assert (!isWrong(bounds.second));
    return bounds;
  }
  
  void BoundAbstractStateDomain::refinementHint (CutPointPtr loc, Expr hint)
  {
    //errs () << "HINT: " << *hint << "\n";
    ExprVector terms;
    ExprVector vars;
    switch (ufocl::HINTS)
      {
	// Adding only variables
      case HIN0:
	errs () << "HIN0\n";
	filter (hint, VarsFilter(), std::back_inserter(vars));
	addVars (loc, vars);
	break;
	// Adding only terms
      case HIN1:
	errs () << "HIN1\n";
	filter (hint, TermsFilter(), std::back_inserter(terms));
	addVars (loc, terms);
	break;
	// Adding both variables and terms
      case HIN2:
	errs () << "HIN2\n";
	filter (hint, TermsFilter(), std::back_inserter(terms));
	forall (Expr e, terms)
	  if (isOp<NumericOp>(e)) // is a term
	    filter (e, VarsFilter(), std::back_inserter(vars));
	addVars (loc, terms);
	addVars (loc, vars);
      }    
        
    hints.insert(hint);
  }

  AbstractState BoundAbstractStateDomain::post 
  (const LocLabelStatePairVector &pre, Loc dst, BoolVector &deadLocs)
  {
    AbstractState res = bot (dst);

    rev_forall (LocLabelStatePair lv, pre)
      {
	Expr val;
	// lv: <loc, <NULL, absstate>> or <loc, <Expr, NULL>>
	if (lv.second.second) val = gamma (lv.first, lv.second.second);
	else val = lv.second.first;
	assert(lv.second.second || lv.second.first);

	// Do the actual post computation
	AbstractState x = exprPost (val, lv.first, dst);

	if (isBotV(x)) errs() << "FALSE_exprpost" <<"\n";
	// Some optimizations can be applied here.
	res = join (dst, res, x);
      }

    // Print out results.
    if (isTopV (res))
      errs () << "RESULT: TOP\n";
    else if (isBotV (res))
      errs () << "RESULT: BOT\n";
    else
      forall (Expr var, getVars(dst))
	{
	  errs () << "RESULT: " << *var << "[";
	  if (getBounds(res, var).isTrueI(var))
	    errs() << "TRUE";
	  else if (getBounds(res, var).isFalseI(var))
	    errs() << "FALSE";
	  else errs()  << getBounds(res, var).first 
		       << ", " << getBounds(res, var).second;
	  errs () << "]\n";
	}

    return res;
  }
  
  AbstractState BoundAbstractStateDomain::exprPost (Expr pre, 
						    CutPointPtr src, 
						    CutPointPtr dst)
  {
    edgeIterator it = src->findSEdge (*dst);
    assert (it != src->succEnd ());
    SEdgePtr edge = *it;

    Environment env (efac);
    // XXX the edge part of the antecedent can be pre-computed. Ask
    // Arie how to do this when everything else works.
    Expr ant = SEdgeCondComp::computeEdgeCond (efac, env, pre, edge, lbe);

    // pass in variables appeared in destination hint 
    ExprSet eVars;
    // -- evaluate variables in the current environment

    // XXX array eVars can be maintained along getVars(dst) and can
    // never change. One solution is to maintain a map from vars to evars
    // 
    forall (Expr var, getVars (dst))
      {
	Expr e_var = env.eval (var);
	eVars.insert (e_var);
	evar2var[e_var] = var;
      }

    VarBound varb (efac, eVars, cache);
    BoxBound<ExprApronBox> boxb (efac, eVars);
    BoxBound<ExprApronOct> boxb2 (efac, eVars);
    BoxBound<ExprApronPk> box3 (efac, eVars);
    DNFBound<ExprApron> dnfb (efac, eVars, ant);

    VarBounds vbs;
    switch (ufocl::UFO_BOUND)
      {
      case BOU0:
	varb.boundAll (ant);
	forall (Expr evar, eVars)
	  vbs[uneval(evar)] = varb[evar];
	break;
      case BOU1:
	switch (ufocl::APLIB)
	  {
	  case ABOX:
	    boxb.boundAll (ant);
	    forall (Expr evar, eVars)
	      vbs[uneval(evar)] = boxb[evar];
	    break;
	  case AOCT:
	    boxb2.boundAll (ant);
	    forall (Expr evar, eVars)
	      vbs[uneval(evar)] = boxb2[evar];
	    break;
	  case APK:
	    box3.boundAll (ant);
	    forall (Expr evar, eVars)
	      vbs[uneval(evar)] = box3[evar];
	  }
	break;
      case BOU2:
	dnfb.boundAll (ant);
	forall (Expr evar, eVars)
	  vbs[uneval(evar)] = dnfb[evar];
      }
                
    return absState(vbs);
  }
  

  void print(Expr e) 
  { 
    cout << e << endl; 
    asm ("");
  }

  void print(Interval i)
  {
    cout << "[" << i.first << "," << i.second << "]" << endl;
  }

  void print(mpq_class m)
  {
    cout << m << endl;
  }

  /*
    void print(AbstractState s)
    {
    for (VarBounds::const_iterator it = 
    getValue(s).begin(); it!= getValue(s).end(); ++it)
    {
    print(it->first);
    print(it->second);
    }

    cout << "Abs State: " << getValue(s).size() << endl;
    }
  */
}
