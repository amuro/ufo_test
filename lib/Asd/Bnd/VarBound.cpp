#include "ufo/Asd/Bnd/VarBound.hpp"

namespace ufo
{
  
  VarBound::~VarBound () {}
  
  /** 
   * Scan linear constraints appeared in formula.
   * Relate vars appeared in the same constraint.
   */
  void VarBound::scanCons (Expr e)
  { 
    ExprSet consbase; // Set of linear constraints
    filter (e, ConsFilter (), std::inserter(consbase, consbase.begin()));

    // Debug for apron expr2ap
    //ExprApron ap (efac, collectVars(e));
    //ap_state s = ap.expr2ap (e);
    //ap.print (s);
    
    foreach(Expr eq, consbase)
    {
      // Insert constraint to edge set
      EQP.insert (mk<EQ>(eq->left(), eq->right()));
      // Relate vars appeared in the same constraint.
      ExprSet vars = collectVars (eq);
      if (vars.size() < 2) continue;

      foreach (Expr var, vars)
      {
        if (var != *(vars.begin()))
        {
          // Relate var with vars.begin
          equi.relate (var, *(vars.begin()));
        }
      }      
    }
  } 

  /**
   * Searching algorithm.
   * Search bounds for terms in the work list. 
   */
  void VarBound::searchBound (Expr phi)
  {
    while (W.size() > 0)
    {
      Expr t = pick (W);
      Stats::count ("varbound.call");

      // COI cache
      if (cache.isHit (t, phi))
      {
        bound(t) = cache.read (t, phi);
        Stats::count ("varbound.cache");
      }
      else
      {
        Stats::count ("varbound.lubcall");
        boost::timer _t;
        Stats::resume ("varbound.lub");
        lub<UpdateGuess> (phi, t, bound(t), bound(t));
        Stats::stop ("varbound.lub");
        Stats::avg ("varbound.avglub", _t.elapsed());

        cache.write (t, phi, bound(t)); 
      }

      solved (t); // term t is solved
        //addTerm (synthesis(), W); // add new synthetic term to W.
    }
  }

  /** 
   * BoundAll. To be called by Bound abstract domain.
   * param[in] formula phi
   * Side Effect: vbs is updated with final bounds value
   */
  typedef std::pair<Expr, Term> TermPair;
  void VarBound::boundAll (Expr phi)
  {
    Stats::resume ("varbound");
    init(phi);

    // Bound Boolean variables.
    boundBool (phi, boolVars, vbs);
    // Add variables to worklist
    foreach (Expr var, realVars)
    {
      Expr negvar = mk<UN_MINUS>(var);
      addTerm (var, W);
      addTerm (negvar, W);
      equi.relate (var, negvar);
    }

    // Add synthetic term.
    //optional<Term> x = synthesis();
    //if (x) 
    //  addTerm (*x, W);

    searchBound (phi);

    Stats::stop ("varbound");
  }

  /**
   * Synthesis term.
   * Input: N.A
   * return: a synthetic term (optional)
   */
  optional<Term> VarBound::synthesis ()
  {
    // Generate a synthetic term
    // one heuristic:
    // combine (+/-) several vars based on their trends. 

    // No term to return.
    if (W.size() < 2)
      return optional<Term>();

    Expr x = pick (W);
    if (!x || isSynthetic (x)) 
      return optional<Term>();

    // Sum all terms.
    vector<Expr> names;
    names.push_back (x);

    ExprSet depTerms;

    // Calculate dependencies
    foreach (TermPair p, W)
    {
      if (isSynthetic (p.first) ||
          equi.isRelated (x, p.first)) 
        continue;

      names.push_back (p.first);
      ExprSet deps = equi.equiClass (p.first);
      foreach (Expr t, deps)
      {
        depTerms.insert (t);
      }
    }

    if (names.size() < 2)
      return optional<Term>();

    Expr t = mknary<PLUS>(mkTerm(mpq_class(0), efac), 
        names.begin(), names.end());

    return optional<Term>(Term (t, depTerms, true, false));
  }

  /**
    Pick term.
    param[in] WorkList w
    return: first unsolved increasing term in the work list
   */
  Expr VarBound::pick (WorkList &W)
  {
    // one heuristic:
    // return the first unsolved increasing term.
    Expr res = NULL;
    rev_forall (TermPair p, W)
    {
      res = p.first;

      if (p.second.increasing)
        break;
    }
    assert (res);
    return res;
  }
}
