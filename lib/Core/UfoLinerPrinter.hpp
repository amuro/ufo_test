#ifndef __UFO_LINER_PRINTER__HPP_
#define __UFO_LINER_PRINTER__HPP_
#include "ufo/ufo.hpp"

#include "llvm/Support/InstVisitor.h"
#include "llvm/Support/raw_ostream.h"

#include "ufo/EdgeComp.hpp"

namespace ufo
{
  template <typename Smt>
  void printLines (raw_ostream &out, const BasicBlock *bb, 
		   Smt &smt, Environment &env)
  {  
    out << "BasicBlock: " << bb->getName () << "\n";
    for (BasicBlock::const_iterator it = bb->begin (),
	   end = bb->end (); it != end; ++it)
      {
	if (const CallInst* ci = dyn_cast<const CallInst> (&*it))
	  {
	    Function *f = ci->getCalledFunction ();
	    if (f == NULL) continue;
	    if (!f->getName ().equals ("__UFO_liner")) continue;
	    
	    const ConstantInt *line = 
	      dyn_cast<const ConstantInt> (ci->getArgOperand (0));
	    assert (line != NULL);
	    out << "file:" << line->getZExtValue () << ":\n";
	    continue;
	  }
	
	const Instruction *inst = &*it;
	if (isa<BranchInst> (inst)) continue;
	if (!inst->getType ()->isIntegerTy ()) continue;
	
	const Value *val = inst;
	Expr valE = mkTerm (val, env.getExprFactory ());
		if (env.isBound (valE))
	  {
	    Expr valS = env.lookup (valE);
	    Expr value = smt.getModelValue (valS);
	    if (value != valS && !isOpX<NONDET> (value))
	      out << "\t" << *valE << ": " << *value << "\n";
	  }	
      }
  } 
}



#endif
