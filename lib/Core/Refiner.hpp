#ifndef __REFINER_HPP_
#define __REFINER_HPP_

#include <list>
#include <map>

#include "ufo/ufo.hpp"

#include "ufo/Cpg/Lbe.hpp"
#include "ufo/Arg.hpp"

#include "ufo/EdgeComp.hpp"

#include "ufo/Smt/ExprMSat.hpp"

namespace ufo
{
  typedef std::map<Node*,Expr> NodeExprMap;
  typedef std::list<const llvm::BasicBlock*> BbList;
  
  class Refiner
  {
  protected:
    ExprFactory& efac;
    AbstractStateDomain &adom;
    LBE& lbe;
    DominatorTree &DT;

    /** Interpolating procedure */
    ExprMSat &interp;

    ARG& arg;    

    /** conservative refine? */
    bool consRefine;
    
    Node* entryN;
    Node* exitN;

    NodeExprMap labels;

    bool doSimplify;

    NodeExprMap& nodeLabels;

    /** Counterexample as a path over an ARG */
    NodeList argCex;
    /** Counterexample as a path over the CFG */
    BbList cfgCex;


  public:
    Refiner (ExprFactory &fac, 
  	     AbstractStateDomain &dom,
  	     LBE &cpG,
	     DominatorTree &dt,
  	     ExprMSat &msat,
  	     ARG &a, bool _doSimp, 
	     NodeExprMap& nodeLabels_) :
      efac(fac), adom (dom), lbe (cpG), DT(dt), interp (msat),  
      arg (a),
      consRefine (false), entryN(NULL), exitN (NULL), doSimplify(_doSimp),
      nodeLabels(nodeLabels_) {}

    virtual ~Refiner () {}

    void setConsRefine (bool v) { consRefine = v; }
    bool isConsRefine () const { return consRefine; }
    
    
    void setEntry (Node *n) { entryN = n; }
    void setExit (Node *n) { exitN = n; }

    const NodeExprMap &getLabels () const { return labels; }    
    bool hasLabel (Node *n) const { return labels.count (n) > 0; }
    
    Expr getLabel (Node *n) const
    {
      NodeExprMap::const_iterator it = labels.find (n);
      assert (it != labels.end ());
      return it->second;
    }

    const NodeList& getArgCex () const { return argCex; }
    const BbList&  getCfgCex () const { return cfgCex; }
    
    virtual void reset () { labels.clear (); }
    virtual void refine () {}
    

  };  

  
}

#endif
