#include <limits.h>

int nd ();
long ndl();
void foo ();

void cast1 ()
{
  long x;
  int y;
  
  y = nd();
  x = (long) y;
  if (x > 0 && x > INT_MAX + 3L) foo ();
}

void cast2 ()
{
  long x;
  
  x = (long) nd();
  if (x > 0 && x > INT_MAX + 3L) foo ();
}

void cast3 ()
{
  long x;
  
  x = ndl();
  if (x > 0 && x > INT_MAX + 3L) foo ();

}

void cast4 ()
{
  long x;
  int y;
  
  y = ndl();
  x = (long) y;
  if (x > 0 && x > INT_MAX + 3L) foo ();
}
