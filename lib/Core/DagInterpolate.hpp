#ifndef __DAG_INTERPOLATE__H_
#define __DAG_INTERPOLATE__H_

#include <boost/range.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/property_map/property_map.hpp>

#include "ufo/ufo.hpp"
#include "ufo/Expr.hpp"
#include "ufo/ufo_iterators.hpp"

namespace ufo
{
  /** Dag Interpolation Procedure (second version)
   *
   * \tparam Graph boost::Graph from BGL
   * \tparam ItpSmt an interpolating SMT solver
   */
  template <typename Graph, typename ItpSmt>
  class DagInterpolate
  {
  public:
    typedef graph_traits<Graph> Traits;
    typedef typename Traits::vertex_descriptor Vertex;
    typedef typename Traits::edge_descriptor Edge;

      

  private:
    struct KillAuxiliary;
    struct CollectOutOfScope;
    struct ZeroOut;
    struct StripVariants;
    
    
    /** An expression factory */
    ExprFactory &efac;
    /** An interpolating smt solver */
    ItpSmt &smt;
    
  public:

    DagInterpolate (ExprFactory &f, ItpSmt &itpSmt) : efac(f), smt (itpSmt) {}
    
    /** Computes a DAG interpolant.
     * 
     * \tparam Range a boost::range
     * \tparam EdgeMap a property map frome Edges to Expr
     * \tparam VertexMap a property map from Vertecies to Expr
     * 
     * \param[in] g the input graph
     * \param[in] topo topological order of vertices of g
     * \param[in] emap a property map from edges of g to Expr
     * \param[out] vmap DAG interpolant as a vertex map
     * 
     * \return true if there is an interpolant, false if the formula is
     * satisfiable, and indeterminate if SMT-solver failed to decide the
     * formula.
     */
    template <typename Range, typename EnvMap, 
	      typename EdgeMap, typename VertexLabelMap, typename VertexMap>
    tribool dagItp (const Graph &g, Range &topo, 
		    const EnvMap &envMap, 
		    const EdgeMap &emap, 
		    const VertexLabelMap &vmap,
		    VertexMap &out)
    {
      // -- encode verification condition of the DAG
      ExprVector vc (distance (topo));
      encodeVC (efac, g, topo, emap, vc.begin ());

      typedef boost::tuple<Node*, Expr&> VE;
      foreach (VE ve, 
	       make_pair
	       (mk_zip_it (++begin (topo), ++begin (vc)),
		mk_zip_it (--end (topo), --end (vc))))

	{
	  Node *v = ve.get <0> ();
	  Expr &e = ve.get <1> ();
	  e = boolop::land (get (vmap, v), e);
	}
      

      // errs () << "VC\n";
      // foreach (Expr e, vc) errs () << *boolop::pp (e) << "\n";

      // -- path-interpolant is stored here
      ExprVector pitp;
      // -- compute interpolants
      tribool res = smt.interpolate (vc.begin (), vc.end (), 
				     std::back_inserter (pitp));
      
      // errs () << "res is " << res << "\n"
      // 	      << "Interpolants found: " << pitp.size () << "\n";
      
      // -- clean the path-interpolant and populate the output
      if (res) cleanPathItp (pitp, g, topo, envMap, emap, out);    
      return res;
    }
    
  private:

    
    
    /**
     * Converts a path-interpolant into a DAG interpolant by eliminating
     * out-of-scope variables.
     * 
     * \tparam Range a boost::range
     * \tparam EdgeMap a property map frome Edges to Expr
     * \tparam VertexMap a property map from Vertecies to Expr
     * 
     * \param[in] g the input graph
     * \param[in] topo topological order of vertices of g
     * \param[in] emap a property map from edges of g to Expr
     * \param[out] vmap DAG interpolant as a vertex map
     * 
     */
    template <typename Range, typename EnvMap, 
	      typename EdgeMap, typename VertexMap>  
    void cleanPathItp (const ExprVector &itp, const Graph &g,
		       Range &topo, 
		       const EnvMap &envMap,
		       const EdgeMap &emap, 
		       VertexMap &vmap)
    {
      put (vmap, *(begin(topo)), mk<TRUE> (efac));
      
      // -- for each (vertex, expression) in  (topological order, itp)
      typedef boost::tuple<Vertex, const Expr&> VE;
      foreach (VE ve, 
	       make_pair
	       (make_zip_iterator (make_tuple (++begin (topo), begin (itp))),
		make_zip_iterator (make_tuple (end (topo), end (itp)))))
	{
	  Vertex v = ve.get<0> ();
	  const Expr &eit = ve.get<1> ();

	  assert (eit);
	  assert (!isRoot (v, g));
	  
	  Expr e = eit;

	  // errs () << "Dirty for " << v->getId () << "\n"
	  // 	  << *boolop::pp (e) << "\n";
	  
	  Expr vTerm = mkTerm (v, efac);

	  {
	    KillAuxiliary ka (vTerm);
	    e = replaceSimplify (e, mk_fn_map (ka));
	  }
	  
	  
	  // env == environment at v
	  const Environment &env = get (envMap, v);
	  CollectOutOfScope co (env);
	  dagVisit (co, e);
	  
	  {
	    // -- zero-out don't cares
	    ZeroOut zo (co.res);
	    e = replaceSimplify (e, mk_fn_map (zo));
	  }


	  e = z3_lite_simplify (e);

	  // -- strip variants
	  StripVariants sv;
	  e = replace (e, mk_fn_map (sv));

	  // errs () << "Clean for " << v->getId () << "\n"
	  // 	  << *boolop::pp (e) << "\n";
	  
	  put (vmap, v, e);
	}	    
    }
    


    /** Substitution used by mkInScope to eliminate all auxilary
	encoding variables */
    struct KillAuxiliary
    {
      /** Node that is set to true */
      Expr nodeE;

      Expr trueE;
      Expr falseE;

      KillAuxiliary (const KillAuxiliary &o) : 
	nodeE(o.nodeE), trueE (o.trueE), falseE (o.falseE) {}
      
      KillAuxiliary (Expr n) : nodeE (n)
      { 
	trueE = mk<TRUE> (nodeE->efac ());
	falseE = mk<FALSE> (nodeE->efac ());
      }

      Expr operator() (Expr e) const 
      {
	// -- set our node to true
	if (e == nodeE) return trueE;
	// -- set all other nodes to false
	if (isOpX<NODE> (e)) return falseE;

	// -- set all edge variables to false
	if (isEdgeVar (e)) return falseE;

	return Expr (0);
      }

      
      bool isEdgeVar (Expr e) const
      {
	if (!bind::isBoolVar (e)) return false;
	Expr name = bind::name (e);
	return isOpX<TUPLE> (name) && name->arity () == 2 &&
	  isOpX<NODE> ((*name) [0]) && isOpX<NODE> ((*name) [1]);
      }
      
    };
      
    struct ZeroOut
    {
      ExprSet &vars;
      ZeroOut (ExprSet &v) : vars(v) {}
    
      Expr operator() (Expr e) const 
      {
	// -- not our variable, do nothing
	if (vars.find (e) == vars.end ()) return Expr (0);
      
	ExprFactory &efac = e->efac ();
      
	Expr mv = e;
	if (isOpX<VARIANT> (mv)) mv = variant::mainVariant (mv);
	if (isOpX<VARIANT> (mv)) mv = variant::mainVariant (mv);
      
	const Value *c = getTerm<const Value*> (mv);
	assert (c != NULL);
      
	return isBoolType (c->getType ()) ?
	  mk<FALSE> (efac) : mkTerm (mpq_class (0), efac);	
      }
    };
  
    struct StripVariants 
    {
      Expr operator () (Expr e) const
      {
	Expr res;
	// -- strip a variant
	if (isOpX<VARIANT> (e)) 
	  res = variant::mainVariant (e);
	// -- double-variant
	if (res && isOpX<VARIANT> (res))
	  res = variant::mainVariant (res);
	  
	return res;
      }
	
    };
      
    /** A visitor to find all variables out-of-scope of a given
	environment. Used by Refiner::mkInScope */
    struct CollectOutOfScope 
    {
      /** the environment */
      const Environment &env;
      /* place to store the result */
      ExprSet res;

    private:
      CollectOutOfScope (const CollectOutOfScope &o) :
	env (o.env), res (o.res) { assert (0); }

    public:
      CollectOutOfScope (const Environment &e) : env(e) {}

      VisitAction operator() (Expr exp)
      {	
	if (isOpX<VARIANT>(exp))
	  {
	    if (res.count (exp) > 0) return VisitAction::skipKids ();

	    Expr mv = variant::mainVariant(exp);
	    if (env.has_binding (mv, exp)) return VisitAction::skipKids ();
	    
	    if (isOpX<VARIANT> (mv)) 
	      { 
		mv = variant::mainVariant(mv);
		if (env.has_binding (mv, exp)) 
		  return VisitAction::skipKids ();
	      }

	    // -- no prime and unprime binding. out of scope
	    res.insert (exp);
	    return VisitAction::skipKids ();
	  }

	return VisitAction::doKids();
      }
    };
  };
}
#endif
