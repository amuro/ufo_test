#ifndef __EXPR_VISIT_H_
#define __EXPR_VISIT_H_

#include "ufo/EdgeComp.hpp"

namespace ufo
{

  /** Collects assumptions appearing in expression */
  struct collectAssumptions
  {
    ExprVector& assumptions;

    VisitAction operator() (Expr exp) 
    { 
      if (isOpX<ASM>(exp)){
        // if (isOpX<NODE>(exp->left()))
        //   assumptions.insert(assumptions.begin(), exp);
        // else 
        assumptions.push_back(exp);
      }

      return VisitAction::doKids (); 
    }

    collectAssumptions(ExprVector& assum):
      assumptions(assum) {}
  };

  struct addAssumptions
  {
    VisitAction operator() (Expr exp) const
    { 
      if (!(isOpX<AND>(exp) || isOpX<OR>(exp) || isOpX<IMPL>(exp)
            || isOpX<XOR>(exp)))
        return VisitAction::changeTo (mk<IMPL>(mk<ASM>(exp), exp));

      return VisitAction::doKids (); 
    }

    addAssumptions()  {}
  };

  struct PlaceAssumptions 
  {
    Expr operator() (Expr exp) const
    {
      Expr res;
      // -- do nothing for AND and OR. 
      if (isOpX<AND> (exp) || isOpX<OR> (exp)) return Expr();

      // -- otherwise, place an assumption for the current subformula
      return mk<IMPL> (mk<ASM> (exp), exp);
    }

  };


  /** replaces assumptions in trueAssumptions 
    with true and others to false 
   */
  struct replaceAssumptions
  {
    ExprSet& trueAssumptions;

    Expr trueE;
    Expr falseE;

    VisitAction operator() (Expr exp) const
    { 
      if (isOpX<ASM>(exp)){
        if (trueAssumptions.count(exp) > 0)
          return VisitAction::changeTo(trueE);
        else
          return VisitAction::changeTo(falseE);
      }

      return VisitAction::doKids (); 
    }

    replaceAssumptions(ExprSet& assumSet, Expr t, Expr f): 
      trueAssumptions(assumSet), trueE(t), falseE(f) {}

  };

  struct KillAssumptions 
  {
    const ExprSet &trueA;

    KillAssumptions (const ExprSet &good) : trueA (good) {}
    KillAssumptions (const KillAssumptions &other) : 
      trueA(other.trueA), 
      cachedFalseE (other.cachedFalseE), cachedTrueE (other.cachedTrueE) {}
    
    

    Expr operator() (Expr exp) const
    {
      Expr res(0);
      // -- if ASM, either change it to false, or leave unchanged
      if (isOpX<ASM> (exp)) 
        res = trueA.count (exp) <= 0 ? falseE (exp) : trueE (exp);

      // -- don't descend into non-boolean parts of the expression
      else if (!isOp<BoolOp> (exp) && !isOpX<TUPLE> (exp)) res = exp;

      // -- return null to indicate that children must be processed
      return res;
    }


    mutable Expr cachedFalseE;
    Expr falseE (Expr exp) const
    {
      if (!cachedFalseE) cachedFalseE = mk<FALSE> (exp->efac ());
      return cachedFalseE;
    }

    mutable Expr cachedTrueE;
    Expr trueE (Expr exp) const
    {
      if (!cachedTrueE) cachedTrueE = mk<TRUE> (exp->efac ());
      return cachedTrueE;
    }


  };

  struct TurnAssumptionsFalse 
  {
    const ExprSet &skip;

    TurnAssumptionsFalse (const ExprSet &trueAssume) : skip (trueAssume) {}

    Expr operator() (Expr exp) const
    {
      Expr res;
      // -- if ASM, either change it to false, or leave unchanged
      if (isOpX<ASM> (exp)) 
        res = skip.count (exp) <= 0 ? falseE (exp) : exp;

      // -- don't descend into non-boolean parts of the expression
      else if (!isOp<BoolOp> (exp)) res = exp;

      // -- return null to indicate that children must be processed
      return res;
    }

    mutable Expr cachedFalseE;
    Expr falseE (Expr exp) const
    {
      if (!cachedFalseE) cachedFalseE = mk<FALSE> (exp->efac ());
      return cachedFalseE;
    }
  };


  /** replaces assumptions not in trueAssumptions
    with false
   */
  struct replaceAssumptionsFalse
  {
    ExprSet& trueAssumptions;

    Expr trueE;
    Expr falseE;

    VisitAction operator() (Expr exp) const
    { 
      if (isOpX<ASM>(exp)){
        if (trueAssumptions.count(exp) <= 0)
          return VisitAction::changeTo(falseE);
      }

      return VisitAction::doKids (); 
    }

    replaceAssumptionsFalse(ExprSet& assumSet, Expr t, Expr f): 
      trueAssumptions(assumSet), trueE(t), falseE(f) {}

  };

  /** replaces assumptions not in trueAssumptions
    with false if they are not in nodes 
   */
  struct replaceAssumptionsFalse_NotNode
  {
    ExprSet& trueAssumptions;

    Expr trueE;
    Expr falseE;

    VisitAction operator() (Expr exp) const
    { 
      if (isOpX<ASM>(exp)){
        if (trueAssumptions.count(exp) <= 0 && !isOpX<NODE>(exp->left()))
          return VisitAction::changeTo(falseE);
      }

      return VisitAction::doKids (); 
    }

    replaceAssumptionsFalse_NotNode(ExprSet& assumSet, Expr t, Expr f): 
      trueAssumptions(assumSet), trueE(t), falseE(f) {}

  };


  // -- takes a formula and updates variables as per env
  struct updateFormula
  {
    // -- Environment passed
    Environment& env;

    VisitAction operator() (Expr exp) const
    { 
      // -- in case a variable (not a constant)
      if (isOpX<BB>(exp) || 
          isOpX<VALUE>(exp) || 
          isOpX<NODE>(exp) || 
          isOpX<VARIANT> (exp)){
        return VisitAction::changeTo(env.lookup(exp));
      }
      return VisitAction::doKids (); 
    }

    updateFormula(Environment& e): env(e) {}
  };    



  // -- takes a formula and gets rid of Booleans
  // -- as well as non-interval constraints
  struct cleanForBoxes
  {
    Expr trueE;
    Expr falseE;

    VisitAction operator() (Expr exp) const
    {
      Expr e = exp;
      bool neg = isOpX<NEG>(exp);

      if (neg) e = exp->left();

      // -- boolean value
      if (isOpX<VALUE>(e)) return VisitAction::skipKids ();


      assert (!isOpX<BB>(e));
      assert (!isOpX<NODE>(e));
      assert (!isOpX<INT>(e));
      //assert (!isOpX<CONST_INT>(e));

      // -- check NNF
      if (neg) 
        //std::cout << "clean for boxes problem: " << e << "\n";
        assert(isOpX<LEQ>(e) || isOpX<GEQ>(e) || isOpX<LT>(e)
            || isOpX<GT>(e) || isOpX<EQ>(e) || isOpX<NEQ>(e));


      // -- if not numeric term, do kids
      if (! (isOpX<LEQ>(e) || isOpX<GEQ>(e) || isOpX<LT>(e)
            || isOpX<GT>(e) || isOpX<EQ>(e) || isOpX<NEQ>(e)))
        return VisitAction::doKids ();


      Expr lhs = e->left();
      Expr rhs = e->right();

      // -- one of the two sides must be a numeral
      if (!((isOpX<MPQ>(lhs) || isOpX<MPQ>(rhs)) || 
            isOpX<MPZ>(lhs) || isOpX<MPZ>(rhs)))
        return VisitAction::changeTo (trueE);

      // -- if no side is VALUE, cannot handle
      if (!(isOpX<VALUE>(lhs) || isOpX<VALUE>(rhs)))
        return VisitAction::changeTo(trueE);

      if (!neg) return VisitAction::skipKids();

      // -- flip operator
      Expr res;
      if (isOpX<LEQ>(e))
        res = mk<GT>(e->left(), e->right());
      if (isOpX<GEQ>(e))
        res = mk<LT>(e->left(), e->right());
      if (isOpX<GT>(e))
        res = mk<LEQ>(e->left(), e->right());
      if (isOpX<LT>(e))
        res = mk<GEQ>(e->left(), e->right());
      if (isOpX<EQ>(e))
        res = mk<NEQ>(e->left(), e->right());
      if (isOpX<NEQ>(e))
        res = mk<EQ>(e->left(), e->right());
      assert (res);

      return VisitAction::changeTo(res);
    }

    cleanForBoxes(Expr trueE_, Expr falseE_): 
      trueE(trueE_), falseE(falseE_) {}
  };    

  struct SetToConst{
    Environment* env;
    Expr falseE;
    Expr zeroE;
    bool outOfScope;
    VisitAction operator() (Expr exp)
    {
      if (isOpX<VARIANT>(exp))
      {

        Expr mv = variant::mainVariant(exp);

        if (env->has_binding (mv, exp)) return VisitAction::skipKids();


        if (isOpX<BB>(mv)) 
          return VisitAction::changeTo(falseE);

        // -- mv could be primed
        if (isOpX<VARIANT> (mv)) 
        { 
          mv = variant::mainVariant (mv); 
          // -- need to check if bound again
          if (env->has_binding (mv, exp)) 
            return VisitAction::skipKids ();
        }

        outOfScope = true;	    

        const Value* c = 
          dynamic_cast<const VALUE&> (mv->op ()).get ();

        assert (c != NULL && "can't be null");

        return  (c->getType ()->isIntegerTy (1)) ?
          VisitAction::changeTo(falseE) :
          VisitAction::changeTo(zeroE);
      }
      return VisitAction::doKids();
    }

    SetToConst(Environment* e, Expr t, Expr o) : 
      env(e), falseE(t), zeroE(o), outOfScope(false) {}
  };

  /** expectes NNF form
   */
  struct boxBooleanHandler
  {
    Expr trueE;
    Expr falseE;
    ExprFactory& efac;

    boxBooleanHandler(Expr t, Expr f, ExprFactory& efaci) : 
      trueE(t), falseE(f), efac(efaci) {}

    VisitAction operator() (Expr exp) 
    {
      bool neg = isOpX<NEG>(exp);
      Expr e;
      if (neg)
        e = exp->left();
      else e = exp;


      if (isOpX<LEQ>(e) || isOpX<GEQ>(e) || isOpX<LT>(e)
          || isOpX<GT>(e) || isOpX<EQ>(e) || isOpX<NEQ>(e)){
        if (isOpX<VALUE>(e->left())){
          const Value* v = 
            dynamic_cast<const VALUE&> (e->left()->op ()).get ();

          if (isBoolType(v->getType())){
            const MPQ& n = dynamic_cast<const MPQ&>(e->right()->op());
            mpz_class numGmp = n.get();
            long num = numGmp.get_si();

            // -- less than or eq <= 
            if ((isOpX<LEQ>(e) && !neg) || (isOpX<GT>(e) && neg)){
              if (num <= 0) return VisitAction::changeTo(mk<NEG>(e->left()));
              else assert(0);//return VisitAction::changeTo(trueE); // -- nondet
            }

            // -- greater than > 
            if ((isOpX<LEQ>(e) && neg) || (isOpX<GT>(e) && !neg)  ){
              if (num >= 0) return VisitAction::changeTo(e->left());
              else assert(0);//return VisitAction::changeTo(trueE); // -- nondet
            }

            // -- less than <
            if ((isOpX<LT>(e) && !neg) || (isOpX<GEQ>(e) && neg)){
              if (num <= 1) return VisitAction::changeTo(mk<NEG>(e->left()));
              else assert(0);//return VisitAction::changeTo(trueE);
            }

            // -- greater than or equal >=
            if ((isOpX<LT>(e) && neg) || (isOpX<GEQ>(e) && !neg)){
              if (num >= 1) return VisitAction::changeTo(e->left());
              else assert(0);//return VisitAction::changeTo(trueE);
            }

            assert(0);
          }else 
            return VisitAction::skipKids();
        }else 
          return VisitAction::skipKids();
      }

      return VisitAction::doKids();
    }
  };


  struct PropSimplifierNNF
  {
    Expr trueE;
    Expr falseE;
    ExprFactory& efac;

    PropSimplifierNNF(Expr t, Expr f, ExprFactory& efaci) : 
      trueE(t), falseE(f), efac(efaci) {}

    VisitAction operator() (Expr exp) 
    {
      if (! isOpX<NEG> (exp)) return VisitAction::doKids ();

      if (isOpX<TRUE>(exp->left()))
        return VisitAction::changeTo (falseE);
      if (isOpX<FALSE>(exp->left()))
        return VisitAction::changeTo (trueE);

      // !! x -> &x
      if (isOpX<NEG> (exp->left ()))
      {
        ExprVector args;
        args.push_back (exp->left()->left());
        // --unary AND
        return 
          VisitAction::changeDoKids(mknary<AND>(args.begin(), args.end()));
      }

      if (isOpX<AND>(exp->left())) 
      {
        Expr e = exp->left ();
        ExprVector args;
        for (ENode::args_iterator it = e->args_begin (), 
            end = e->args_end (); it != end; ++it)
          args.push_back (boolop::lneg (*it));
        return VisitAction::changeDoKids(mknary<OR>(args.begin(), 
              args.end()));
      }

      if (isOpX<OR>(exp->left()))
      {
        Expr e = exp->left();
        ExprVector args;
        for (ENode::args_iterator it = e->args_begin (), 
            end = e->args_end (); it != end; ++it)
          args.push_back (boolop::lneg (*it));
        return VisitAction::changeDoKids(mknary<AND>(args.begin(),
              args.end()));
      }

      return VisitAction::doKids ();
    }
  };

  struct PropSimplifier
  {
    Expr trueE;
    Expr falseE;
    ExprFactory& efac;

    PropSimplifier(Expr t, Expr f, ExprFactory& efaci) : 
      trueE(t), falseE(f), efac(efaci) {}

    Expr operator() (Expr exp) 
    { 

      if (isOpX<IMPL>(exp))
      {
        // TRUE -> x  == x
        if (trueE == exp->left ()) return exp->right();

        // FALSE -> x == TRUE
        if (falseE == exp->left ()) return trueE;

        // x -> TRUE == TRUE
        if (trueE == exp->right ()) return trueE;

        return exp;
      }

      if (isOpX<NEG>(exp))
      {
        if (trueE == exp->left()) return falseE;
        if (falseE == exp->left()) return trueE;
        if (isOpX<NEG> (exp->left ())) return exp->left ()->left ();
        if (isOpX<AND>(exp->left()))
        {
          Expr e = exp->left();
          std::vector<Expr> args;
          for (ENode::args_iterator it = e->args_begin (), 
              end = e->args_end (); it != end; ++it){
            if (isOpX<NEG>(*it))
              args.push_back((*it)->left());
            else
              args.push_back(mk<NEG>(*it));
          }
          assert(args.size() != 1);
          return mknary<OR>(args.begin(), args.end());
        }
        if (isOpX<OR>(exp->left()))
        {
          Expr e = exp->left();
          std::vector<Expr> args;
          for (ENode::args_iterator it = e->args_begin (), 
              end = e->args_end (); it != end; ++it){
            if (isOpX<NEG>(*it))
              args.push_back((*it)->left());
            else
              args.push_back(mk<NEG>(*it));
          }
          assert(args.size() != 1);
          return mknary<AND>(args.begin(), args.end());
        }

        return exp;
      }

      if (isOpX<OR>(exp))
      {
        // -- special case
        if (exp->arity() == 1) return exp->left();

        if (exp->arity () == 2)
        {
          Expr lhs = exp->left ();
          Expr rhs = exp->right ();

          if (lhs == rhs) return lhs;
          if (trueE == lhs || trueE == rhs) return trueE;
          if (falseE == lhs) return rhs;
          if (falseE == rhs) return lhs;
          // (!a || a)
          if (isOpX<NEG>(lhs) && lhs->left () == rhs) return trueE;
          // (a || !a)
          if (isOpX<NEG>(rhs) && rhs->left () == lhs) return trueE;
          if (! (isOpX<OR>(lhs) || isOpX<OR>(rhs))) return exp;
        }

        // XXX The next part agregates binary operators into an
        // n-ary operator for readability
        std::set<Expr> newArgs;
        for (ENode::args_iterator it = exp->args_begin (), 
            end = exp->args_end (); it != end; ++it)
        {
          Expr arg = *it;
          if (! isOpX<OR> (arg)) 
          {
            if (arg == trueE) return trueE;
            if (arg != falseE) newArgs.insert (arg);
          }
          else
          {
            for (ENode::args_iterator argit = arg->args_begin (),
                argend = arg->args_end (); argit != argend; ++argit)
            {
              Expr t = *argit;
              if (t == trueE) return trueE;
              if (t != falseE) newArgs.insert (t);
            }
          }

        }
        if (newArgs.size () == 0)
          return falseE;
        if (newArgs.size () == 1)
          return *(newArgs.begin ());

        return mknary<OR> (newArgs.begin (), newArgs.end ());

      }

      if (isOpX<AND>(exp)){
        // -- special case
        if (exp->arity() == 1) return exp->left();

        if (exp->arity () == 2)
        {
          Expr lhs = exp->left ();
          Expr rhs = exp->right ();

          if (lhs == rhs) return lhs;
          if (falseE == lhs || falseE == rhs) return falseE;
          if (trueE == lhs) return rhs;
          if (trueE == rhs) return lhs;
          if (isOpX<NEG>(lhs) && lhs->left () == rhs) return falseE;
          if (isOpX<NEG>(rhs) && rhs->left () == lhs) return falseE;

          if (! (isOpX<AND>(lhs) || isOpX<AND>(rhs))) return exp;
        }

        // XXX Same comment as for OR

        std::set<Expr> newArgs;
        for (ENode::args_iterator it = exp->args_begin (), 
            end = exp->args_end (); it != end; ++it)
        {
          Expr arg = *it;
          if (! isOpX<AND> (arg)) 
          {
            if (arg == falseE) return falseE;
            if (arg != trueE) newArgs.insert (arg);
          }
          else
          {
            for (ENode::args_iterator argit = arg->args_begin (),
                argend = arg->args_end (); argit != argend; ++argit)
            {
              Expr t = *argit;
              if (t == falseE) return falseE;
              if (! (t == trueE)) newArgs.insert (t);
            }
          }

        }
        if (newArgs.size () == 0)
          return trueE;
        if (newArgs.size () == 1)
          return *(newArgs.begin ());

        return mknary<AND> (newArgs.begin (), newArgs.end ());
      }

      // XXX Cut off numeric simplifications (at least for now).
      return exp;

      if (exp->arity() == 2){
        if (isOpX<INT>(exp->left()) && isOpX<INT>(exp->right())){
          Expr op1 = exp->left();
          Expr op2 = exp->right();
          int i1 = boost::lexical_cast<int>(op1.get());
          int i2 = boost::lexical_cast<int>(op2.get());

          if (isOpX<PLUS>(exp))
            return (mkTerm(i1 + i2, efac));
          if (isOpX<MINUS>(exp))
            return (mkTerm(i1 - i2, efac));
          if (isOpX<EQ>(exp)){
            if (i1 == i2)
              return trueE;
            else return falseE;
          }
          if (isOpX<NEQ>(exp)){
            if (i1 != i2)
              return trueE;
            else return falseE;
          }
          if (isOpX<LEQ>(exp)){
            if (i1 <= i2)
              return trueE;
            else return falseE;
          }
          if (isOpX<GEQ>(exp)){
            if (i1 >= i2)
              return trueE;
            else return falseE;
          }
          if (isOpX<LT>(exp)){
            if (i1 < i2)
              return trueE;
            else return falseE;
          }
          if (isOpX<GT>(exp)){
            if (i1 > i2)
              return trueE;
            else return falseE;
          }
          else
            return exp;
        }
      }
      return exp;
    }
  };

  struct Simp
  {
    ExprFactory& efac;
    Expr trueE;
    Expr falseE;

    boost::shared_ptr<PropSimplifier> ps;

    Simp(Expr t, Expr f, ExprFactory& efaci) : 
      efac(efaci), trueE(t), falseE(f), ps(new PropSimplifier (t,f,efaci)) {}

    VisitAction operator() (Expr exp)
    {
      return VisitAction::changeDoKidsRewrite (exp, ps);
    }
  };

  inline Expr exprToNNF(Expr e)
  {
    ExprFactory& efac = e->efac();
    PropSimplifierNNF nnf(mk<TRUE>(efac), mk<FALSE>(efac), efac);
    Simp simp(mk<TRUE>(efac), mk<FALSE>(efac), efac);
    Expr e1 = dagVisit(nnf, e);
    Expr e2 = dagVisit(simp, e1);
    return e2;
  }

  inline Expr cleanBox(Expr e){
    ExprFactory& efac = e->efac();
    Expr e1 = exprToNNF(e);
    boxBooleanHandler bbh(mk<TRUE>(efac), mk<FALSE>(efac), efac);
    return dagVisit(bbh, e1);
  }


}
#endif
