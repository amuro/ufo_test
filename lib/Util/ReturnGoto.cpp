#include "ufo/InitializePasses.h"

#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Instructions.h"
#include "llvm/Function.h"
#include "llvm/BasicBlock.h"
#include "llvm/PassManager.h"
#include <llvm/LLVMContext.h>
#include "llvm/Support/IRBuilder.h"


using namespace llvm;

namespace 
{
  /** Replaces return statements with an infinite loop */
  struct ReturnGoto : public FunctionPass
  {

    static char ID;
    ReturnGoto() : FunctionPass (ID) {}

    virtual bool runOnFunction(Function &F) 
    {
      if (F.getName ().compare ("main") != 0) return false;

      IRBuilder<> B(F.getContext ());
      
      BasicBlock *loop = BasicBlock::Create (F.getContext (), 
					     "_UFO__exit",
					     &F);
      B.SetInsertPoint (loop);
      B.CreateBr (loop);

      for (Function::iterator b = F.begin(), be = F.end(); b != be; ++b)
	{
	  BasicBlock *bb = &*b;
	  TerminatorInst *oldTI = bb->getTerminator ();
	  if (ReturnInst *ret = dyn_cast<ReturnInst> (oldTI))
	    {
	      B.SetInsertPoint (ret);
	      B.CreateBr (loop);
	      ret->eraseFromParent ();
	    }
	}
      return true;
    }
    
    virtual void getAnalysisUsage (AnalysisUsage &AU) const
    {
      // -- don't preserve anything
    }
    
  };
char ReturnGoto::ID = 0;
}

//static RegisterPass<ReturnGoto> X("return-goto", 
//				      "in main(), replaces returns w/ loops");
INITIALIZE_PASS(ReturnGoto, "return-goto", 
		"in main(), replaces returns w/ loops", false, false)
