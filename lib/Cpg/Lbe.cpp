#include "ufo/InitializePasses.h"

#include "ufo/Cpg/Lbe.hpp"
#include "ufo/Cpg/Topo.hpp"
#include "llvm/Function.h"
#include "llvm/Support/InstIterator.h"
#include "llvm/Module.h"
using namespace llvm;

// XXX Not used
//static cl::opt<bool> 
// LBEOpt("LBEOpt",
//        cl::desc("large block encoding"),
//        cl::init(false));
  // XXX Memory leak: CutPoint and SEdge classes are created on the
  // XXX heap and never deleted. Solution: Store a CutPoint by value
  // in the vector cutPts. Then extract references from there. Use
  // boost::smart_ptr to keep track of SEdges


    std::string SEdge::toString() const    
      {
	std::string s = "\nedge:\n";
	for (size_t i = 0; i < bbs.size(); i++)
	  s = s + bbs[i]->getNameStr() + "\n";
	
	s = s + getDst()->getNameStr ();
	s = s + "\nend edge\n";
	return s;
      }

    void LBE::computeCutPts(Function &F)
    {
      errs () << "Computing Cutpoints for " << F.getName() << "\n";
      int i = 0; //cutpoint ID/counter

      //initial block is a cutpoint
      // CutPoint cp (i++, &(F.getEntryBlock()));
      // XXX cp object is destroyed when this function exits
      // XXX cutPts will contain pointer into nowhere!
      // cutPts.push_back(&cp); 
      // bb2cp [cp.bb] = &cp;
      
      CutPointPtr entryCP( new CutPoint(i++, &(F.getEntryBlock())));
      cutPts[&F].push_back (entryCP);
      bb2cp [entryCP->bb] = entryCP;
	     

      //makes sure that there's one exit BB
      UnifyFunctionExitNodes& u = getAnalysis<UnifyFunctionExitNodes>(F);
      
      if (u.getReturnBlock() == NULL){
        errs() << "\n\nLBE: Main does not return\n";
        errs() << "program correct: ERROR unreachable\n";
	errs () << "BRUNCH_STAT Result SAFE\n";
        exit(1);
      }

      // -- final block is a cutpoint
      CutPointPtr exitCP (new CutPoint(i++, u.getReturnBlock()));
      cutPts[&F].push_back(exitCP);
      bb2cp [exitCP->bb] = exitCP;
		
      // needed for access to backedge function	
      Topo &t = getAnalysis<Topo>();

      // iterate over topological ordering
      for (std::vector<const BasicBlock*>::const_iterator it = t.begin(&F),
	     end = t.end (&F); it != end ; ++it)
	{
	  const BasicBlock* bb = *it;
			
	  //special cutpoints are handled above
	  if (bb == &(F.getEntryBlock()) || bb == u.getReturnBlock())	
	    continue;

	  // -- check whether bb is a destination of a back edge
	  for (const_pred_iterator PI = pred_begin(bb), E = pred_end(bb); 
	       PI != E; ++PI) 
	    {
	      const BasicBlock *pred = *PI;
	      if (t.isBackEdge (&F, pred, bb))
		{
		  // XXX How is the memory for this cutpoint reclaimed?
		  CutPointPtr cp ( new CutPoint (i++,bb));
		  cutPts[&F].push_back(cp);
		  bb2cp [cp->bb] = cp;
		  break;
		}		
	    }
	}
    }

    void LBE::computeFwdCutPts(Function &F)
    {
      Topo &t = getAnalysis<Topo>();

      
      // -- iterate over blocks in bwd topological order
      for (std::vector<const BasicBlock*>::const_reverse_iterator it = t.rbegin(&F), 
	     end = t.rend (&F); it != end ; ++it)
	{
	  const BasicBlock* bb = *it;
	  // -- create a new BitVector and grab a reference to it
	  BitVector &r = fwdCutPts [bb];
	  for (succ_const_iterator SI = succ_begin(*it), E = succ_end(*it); 
	       SI != E; ++SI)
	    {
	      const BasicBlock *succ = *SI;
	      DenseMap<const BasicBlock*,CutPointPtr>::iterator cpit
		= bb2cp.find(succ);
	    
	      //if bb is a cutpoint
	      if (cpit != bb2cp.end())
		{
		  CutPointPtr cp = cpit->second;
		  if (r.size () <= (unsigned int) cp->getID ()) r.resize (cp->getID () + 1);
		  //set the bit corresponding to the ID
		  //of the cutpoint to 1
		  r.set (cp->getID ());
		}
	      else
		{
		  assert(fwdCutPts.find(succ) != fwdCutPts.end());
		  if (r.size() < fwdCutPts[succ].size())
			r.resize(fwdCutPts[succ].size());
		  else if (r.size() > fwdCutPts[succ].size())	
			fwdCutPts[succ].resize(r.size());
		  r |= fwdCutPts [succ];
		}
	    }
	}
    }

    void LBE::computeBwdCutPts (Function &F)
    {
      Topo &t = getAnalysis<Topo>();

      //iterate over blocks in forward topological order
      for (std::vector<const BasicBlock*>::const_iterator it = t.begin(&F),
	   end = t.end (&F); it != end ; ++it)
	{
	  const BasicBlock *bb = *it;

	  // allocate a new BitVector and grab a reference to it
	  BitVector &r = bwdCutPts [bb];


	  for (const_pred_iterator PI = pred_begin(bb), E = pred_end(bb); 
	       PI != E; ++PI)
	    {
	      const BasicBlock *pred = *PI;

	      //disregard backedges
	      if (t.isBackEdge(&F, pred, bb)) continue;
	    
	      DenseMap<const BasicBlock*,CutPointPtr>::iterator cpit = bb2cp.find(pred);
	    
	      if (cpit != bb2cp.end())
		{ //if bb is a cutpoint
		  CutPointPtr cp = cpit->second;
		  if (r.size() <= (unsigned int) cp->getID())
		    r.resize (cp->getID () + 1);
		  r.set (cp->getID ());
		}
	      else
		{
		  assert(bwdCutPts.find(pred) != bwdCutPts.end());
		  if (r.size() < bwdCutPts[pred].size())
			r.resize(bwdCutPts[pred].size());
		  else if (r.size() > bwdCutPts[pred].size())	
			bwdCutPts[pred].resize(r.size());
		  r |= bwdCutPts[pred];
		}
	    }
	}

      // -- cutpoints get special treatements due to backedges
      // -- which were disregarded previously
      for (std::vector<CutPointPtr>::iterator cit = cutPts[&F].begin(), 
	     end = cutPts[&F].end (); cit != end; ++cit)
	{
	  const BasicBlock* bb = (*cit)->getBB();
	  BitVector &r = bwdCutPts [bb];
	  
          for (const_pred_iterator PI = pred_begin(bb), E = pred_end(bb); 
	       PI != E; ++PI)
	    {
	      const BasicBlock *pred = *PI;
	      if (! t.isBackEdge(&F, pred, bb)) continue;

	      DenseMap<const BasicBlock*,CutPointPtr>::iterator 
		cpit = bb2cp.find(pred);
	      if (cpit != bb2cp.end())
		{
		  CutPointPtr cp = cpit->second;
		  if (r.size() <= (unsigned int) cp->getID()) r.resize (cp->getID () + 1);
		  r.set (cp->getID ());
		}
	      else
		{
		  assert(bwdCutPts.find(pred) != bwdCutPts.end());
		  if (r.size() < bwdCutPts[pred].size())
			r.resize(bwdCutPts[pred].size());
		  else if (r.size() > bwdCutPts[pred].size())	
			bwdCutPts[pred].resize(r.size());
		  r |= bwdCutPts[pred];

		}
	    }
	}      
    }
    
    void LBE::computeSEdges(Function& F)
    {
      Topo &t = getAnalysis<Topo> ();

      //iterate in fwd topo order...
      for (std::vector<const BasicBlock*>::const_iterator it = t.begin(&F), 
	     end = t.end (&F); it != end ; ++it)
	{

	  const BasicBlock *bb = *it;

	  DenseMap<const BasicBlock*,CutPointPtr>::iterator cpit  = bb2cp.find (bb);
	  
	  // -- if bb is a cutpoint
	  if (cpit != bb2cp.end ())
	    {
	      CutPointPtr cp = cpit->second;
	      BitVector r = fwdCutPts [bb];
	      
	      for (int i = r.find_first(); i >= 0; i = r.find_next(i))
		{
		  // XXX How is memory for this SEdge reclaimed?
		  // XXX Use boost::shared_ptr to wrap SEdge
		  SEdgePtr e(new SEdge (cp, cutPts[&F][i]));
		  (cp->succ).push_back( e );
		  cutPts[&F][i]->pred.push_back ( e );
		  e->bbs.push_back (bb);
		}
	    }
	  else
	    {
	      //if not cutpoint
	      //look for closest reachable cutpoints around bb
	      //and add it to the edge connecting them
	      BitVector fwd = fwdCutPts [bb];
	      BitVector bwd = bwdCutPts [bb];
	      for (int i = bwd.find_first(); i >= 0; i = bwd.find_next(i))
		for (int j = fwd.find_first(); j >= 0; j = fwd.find_next(j))
		  addToEdge (cutPts[&F][i], cutPts[&F][j], bb); 
			//errs () << *bb << "-between- " << i << " and "<< j ;}
	    }
	}
    }
	
    void LBE::addToEdge(CutPointPtr src, CutPointPtr dst, const BasicBlock* bb)
    {
      // -- find edge between src and dest cutpoint and add the bb to that edge
      for (SmallVector<SEdgePtr,3>::iterator it = src->succ.begin (), 
	     end = src->succ.end (); it != end; ++it)
	{
	  if (((*it)->dst).get() == dst.get())
	    {
	      (*it)->bbs.push_back (bb);
	      return;
	    }
	}
    }
    
    // -- XXX assumes function has no dead closed loops - it would get stuck there
    void LBE::getRandFPath(std::vector<SEdgePtr>& path,
                      Function* F) const{
      std::set<CutPointPtr> cpPath;
      Topo &topo = getAnalysis<Topo>();
      
      CutPointPtr li = (bb2cp.find(&(F->getEntryBlock ())))->second;
      cpPath.insert(li);
      
      while (true){
        if (li->succSize() == 0) break;

        for (edgeIterator i = li->succBegin(), 
            e = li->succEnd(); i != e; ++i){
              
              SEdgePtr sedge = *i;

              //if destination has been visited before
              //and it's not a back edge, don't take it.
              if ((cpPath.find(sedge->getDst()) != cpPath.end())
                && !(topo.isBackEdge(F,li->getBB(), sedge->getDst()->getBB())))              
                continue;
              path.push_back(sedge);
              li = sedge->getDst();
              cpPath.insert(li);
              break;
        }
      }
    }

    bool LBE::runOnModule (Module &M) {
      // XXX Choice of function should be CLI argument
      // -- all function call inlined
        errs () << "\n == Running LBE == \n";
        F = M.getFunction("main");
        
        errs () << "F name: " << F->getName() << "\n";
        computeCutPts(*F);
        computeFwdCutPts(*F);
        computeBwdCutPts(*F);
        computeSEdges(*F);
        
        //weakTopologicalOrder(F);
        
                
        // -- Testing code -- prints cutpoints and results of nextWtoHead
        // -- and wtoNextSame
        /*
        for (int i = 0; i < graph->interval.size() - 1; ++i){
          if (graph->interval[i]->getID() < 0 ) continue;
          
          errs() << graph->interval[i]->getBB()->getName(); 
            errs ()<< " has succ: " <<
              
          nextWtoHead(graph->interval[i])->getBB()->getName() << "\n";
          errs () << " Next: " <<
            wtoNextSame(graph->interval[i])->getBB()->getName() << "\n";
        }*/
        errs () << "done\n";
      	        //BasicBlock *blk = &(F.getEntryBlock());
		//for (BasicBlock::iterator i = blk->begin(), 
                //    e = blk->end(); i != e; ++i){
		//	errs () << *i << "\n";
		//}

    /*for (inst_iterator I = inst_begin(F), E = inst_end(F); I != E; ++I){
        errs() << &*I << " : " <<*I << "\n";
        const CallInst* c = NULL;
        
        if ((*I).getOpcode() == ReturnInst::Ret) errs () << 
          "returning : " << (*I).getOperand(0) << "\n";
        if ((*I).getOpcode() == CallInst::Call) errs () << "calling";
        if ((c = dyn_cast<const CallInst>(&(*I))) != NULL){
            errs () << "\t\t" << c->getCalledFunction()->getName() << "\n";
            errs () << c->getNumOperands() << "\n";
            errs () << c->getOperand(0)->getName() << "\n";
            errs () << (cutPts.find(dyn_cast<Function> (c->getOperand(0))) == cutPts.end()); 
            Function* f = dyn_cast<Function> (c->getOperand(0));
            Function::arg_iterator i,e;
            for (i =  f->arg_begin(), e = f->arg_end();
                i != e; i++)
            {
              errs () << "\nargument: " << i << "\n";
            }
        }
      
      }*/
     //     errs() << *i << "\n";
		// Debugging code
//	  errs() << "\n";
        for (unsigned int i=0; i < cutPts[F].size(); i++){
    	errs() << i << "CUTPOINT: " << (cutPts[F][i]->bb)->getName() << "\n\n";
    	CutPointPtr cp = cutPts[F][i];
    		for (unsigned int j=0; j < cp->succ.size(); j++){
    			errs ()<< "--another edge--\n";
			for (size_t k=0; k < (cp->succ[j])->bbs.size(); k++)
				errs() << (((cp->succ[j])->bbs[k]))->getName() << "\n";
			errs ()<< "to: "<<(((cp->succ[j])->getDst()))->getBB()->getName();
			errs () << "\n\n";
		}
	}
      return false;
    }

char LBE::ID = 0;
//static RegisterPass<LBE> 
//X("LBE", "large block encoding");
INITIALIZE_PASS_BEGIN(LBE, "LBE", 
		      "Large Block Encoding", 
		      false, false)
INITIALIZE_PASS_DEPENDENCY(Topo)
INITIALIZE_PASS_DEPENDENCY(UnifyFunctionExitNodes)
INITIALIZE_PASS_END(LBE, "LBE", 
		    "Large Block Encoding", 
		   false, false)
