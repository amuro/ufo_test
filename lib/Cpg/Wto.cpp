#include "ufo/InitializePasses.h"

#include "llvm/Function.h"
#include "llvm/Support/InstIterator.h"
#include "ufo/Cpg/Wto.hpp"
#include "llvm/Module.h"
using namespace llvm;
 
    bool allPredsAreInFixpoint(std::vector<WTOInt*>& fixpoint, WTOInt* n)
    {
      // XXX Does not fit "if !found then false" pattern 
      // -- first check if n is in fixpoint
      forall (WTOInt *fp, fixpoint) if (n == fp) return false;

      // -- then check if ALL of preds of n are in fixpoint

      forall (WTOInt *p, n->getPreds ())
	{
	  bool found = false;

	  forall (WTOInt *fp, fixpoint) 
	    if (p == fp) { found = true; break; }
	  
	  if (!found) return false;
	}
      
      return true;
    }

    bool isProcessed(std::vector<WTOInt*>& processed, WTOInt* n){
      for (std::vector<WTOInt*>::iterator i = processed.begin(),
                e = processed.end(); i != e; ++i){
        if (n->getHead () == (*i)->getHead ()) return true;
      }
      
      return false;
    }

    bool canReach(WTOInt* a, WTOInt* b)
    {
      forall (CutPointPtr cp, a->getInterval ())
	{
	  forall (SEdgePtr edge, *cp)
	    {
	      if (edge->getDst () == b->getHead ()) return true;
	    }
	}

      return false;
    }
    
    bool canReachCP (CutPointPtr cp, CutPointPtr cp2)
    {
      for (edgeIterator i = cp->succBegin(), e = cp->succEnd(); i != e; ++i){
        if ((*i)->getDst() == cp2) return true;
      }
      return false;

    }

    bool canReachInterval (WTOInt* a, WTOInt* b){
      
      forall (CutPointPtr cp, a->getInterval()){
        if (cp->getID() < 0) continue;  
	forall (SEdgePtr edge, *cp){
	  
	  forall (CutPointPtr cpb, b->getInterval()){
	    if (edge->getDst () == cpb) return true;
	  }
	
	}
      
      }

      return false;
    }

    void setBracketSingle(WTOInt* t, CutPointPtr leftB, CutPointPtr rightB)
    {
      if (t->getInterval ()[0]->getID() < 0) return;
      
      CutPointPtr lastCP = leftB;
      
      for (std::vector<CutPointPtr>::reverse_iterator i = 
	     t->getInterval ().rbegin(),
             e = t->getInterval ().rend(); i != e; ++i){
	if ((*i)->getID() < 0) continue;
	if (canReachCP(*i, t->getHead ())){
	  lastCP = *i;
	  //errs() << "\nAdding br around " << t->head->getBB()->getName() << "\n";
	  //t->interval.push_back(rightB);
	  //t->interval.insert(t->interval.begin(),leftB);
           
	  break;
        }
      }
      
      if (lastCP == leftB) return;
      
      int left = 0;
      int right = 0;
      bool found = false;
      for (std::vector<CutPointPtr>::iterator i = t->getInterval ().begin(),
             e = t->getInterval ().end(); i != e; ++i)
	{
	  if ((*i)->getID() == -1) left++;
	  if ((*i)->getID() == -2) right++;
	  if (*i == lastCP) found = true;

	  if (found && (left == right)){
	    ++i;
	    t->getInterval ().insert(i,rightB);
	    t->getInterval ().insert(t->getInterval ().begin(),leftB);  
	    break;
	  }
	}
    }

   /** takes a vector of intervals and adds appropriate brackets */
   void setBrackets(std::vector<WTOInt*>& ints,CutPointPtr leftB, CutPointPtr rightB){
      
      for (std::vector<WTOInt*>::iterator it = ints.begin(),
            et = ints.end(); it != et; ++it){
        WTOInt* t = *it;
        if (t->getInterval ()[0]->getID() < 0) continue;
      
        for (std::vector<CutPointPtr>::iterator i = t->getInterval ().begin(),
	       e = t->getInterval ().end(); i != e; ++i){
          
          if ((*i)->getID() < 0) continue;
          
          if (canReachCP(*i, t->getHead ())){
            t->getInterval ().push_back(rightB);
            t->getInterval ().insert(t->getInterval ().begin(),leftB);
            break;
          }   
        }
      }

      return;
    }

  /** connects WTO intervals, by checking which ones can reach others 
      when init is true, an interval can have an edge to itself, this is used
      when constructing the first interval graph that resembles the CFG 
   */
  
  void buildGraph(std::vector<WTOInt*>& w, bool init){
      for (std::vector<WTOInt*>::iterator i = w.begin(), e = w.end();
          i != e; ++i){
        for (std::vector<WTOInt*>::iterator it = w.begin(), et = w.end();
            it != et; ++it){
          
          if ((i == it) && !init) continue;
          
          if (canReach(*i,*it)){
	    (*i)->addSucc (*it);
	    (*it)->addPred (*i);
          }
        
        }
 
      }
    }

  
  void printIntervals(const std::vector<WTOInt*>& w) 
  {
    forall (WTOInt *iv, w)
    {
      errs () << "==Inteval size " << iv->getInterval ().size() << " ";
      forall (CutPointPtr cp, iv->getInterval ())
	{
	  if (cp->getID() == -1) errs() << "(";
	  else if (cp->getID() == -2) errs () << ")";
	  else errs () << cp->getBB()->getName() << " ";
	}
      errs () << "\n";
    }
  }

    /** first loop in Aho, Sethi, Ullman (1986), p665 */
    void firstLoop(std::vector<WTOInt*>& fixpoint,
                    std::vector<WTOInt*>& processed,
                    CutPointPtr leftB, CutPointPtr rightB,
                    WTOInt* hn){
       bool loop = true;
       std::vector<WTOInt*> toMerge;
       std::vector<WTOInt*> toMergeLater;
       int fsize = fixpoint.size();
       while (loop){
          loop = false;
	  int i = 0;
          while (i < fsize){
            
            WTOInt* curr = fixpoint[i];
            
	    assert (curr != NULL);
            for (std::vector<WTOInt*>::const_iterator it = curr->getSuccs ().begin(),
		   et = curr->getSuccs ().end(); it != et; ++it){
              if (allPredsAreInFixpoint(fixpoint, *it)){
                loop = true;
                
                //if (canReach(*it, hn)){
                if (false){
	       	  errs () << "\nmergingIn " << 
                    (*it)->getHead ()->getBB()->getName() <<"\n";
                  hn->mergeIn(**it);
                }
                else{
                  errs () << "\nmergingOut " << 
                    (*it)->getHead ()->getBB()->getName() <<"\n";
                  //hn->merge(**it);
                  toMerge.push_back(*it);
                }
                
                setBracketSingle(hn, leftB, rightB);
                
                fixpoint.push_back(*it);
		fsize++;
                errs () << "Adding to fixpoint " << 
                  (*it)->getHead ()->getBB()->getName() << "\n";
                processed.push_back(*it);
              }

            }
	    i++;
          }
        }

        for (size_t i = 0; i < toMerge.size(); i++){
           errs () << "\nMergeOut: Checking canReach: " << 
                    toMerge[i]->getHead ()->getBB()->getName() <<"\n";
                  
	  if (canReachInterval(toMerge[i], hn)){
	    errs () << "true \n";
	    hn->mergeIn(*toMerge[i]);
	  }
	  else{
	    errs () << "false \n";
	    toMergeLater.push_back(toMerge[i]);
	  }
	  std::vector<WTOInt*> temp;
	  temp.push_back(hn);
	  setBracketSingle(hn, leftB, rightB);
	  errs () << "After bracketing\n";
	  printIntervals(temp);
	}

        for (size_t i = 0; i < toMergeLater.size(); i++){
	  hn->merge(*toMergeLater[i]);
	  setBracketSingle(hn, leftB, rightB);
	}
        
	toMergeLater.clear();
        toMerge.clear();


    }
    
   /** second loop in Aho, Sethi, Ullman (1986), p665 */
   void secondLoop(std::vector<WTOInt*>& fixpoint,
                    std::vector<WTOInt*>& processed,
                    std::vector<WTOPass::ip>& headers){
        for (std::vector<WTOInt*>::iterator i = fixpoint.begin(),
	       e = fixpoint.end(); i != e; ++i){
            
	  WTOInt* curr = *i;
            
	  for (std::vector<WTOInt*>::const_iterator it = curr->getSuccs ().begin(),
		 et = curr->getSuccs ().end(); it != et; ++it){
	    if (!isProcessed(processed, *it)){
	      WTOInt* itn = new WTOInt(*(*it));
	      headers.push_back(WTOPass::ip(*it,itn));
	      processed.push_back(*it);
	    }
	  }
        }
    }
 
    void WTOPass::addSelfLoops (Function &F, WTOInt* graph){
      
	std::vector<CutPointPtr>& cps = lbe->cutPts[&F];
    	
	for (std::vector<CutPointPtr>::iterator i = cps.begin(),
              e = cps.end(); i != e; ++i){
	   
	   // -- detect self loops
	   if ((*i)->succSize() == 1 &&
	       (*i)->getSucc(0)->getDst() == *i){
	     graph->getInterval().push_back(leftB);
	     graph->getInterval().push_back(*i);
	     graph->getInterval().push_back(rightB);
	   }

         }
    }
    
    /** this creates an interval graph isomorphic to the CFG of F */
    WTOInt* WTOPass::createInitialGraph(Function& F){
	std::vector<WTOInt*> mainGraph;
	std::vector<CutPointPtr>& cps = lbe->cutPts[&F];
	
        // -- makes sure that there's one exit BB
	UnifyFunctionExitNodes& u = getAnalysis<UnifyFunctionExitNodes>(F);
	BasicBlock* ret = u.getReturnBlock();

        for (std::vector<CutPointPtr>::iterator i = cps.begin(),
              e = cps.end(); i != e; ++i){
	   
	   // -- return block is error location ...
	   // -- place it at the end of WTO
           if ((*i)->getBB() ==  ret) continue;
           
	   // -- skip self loops
	   if ((*i)->succSize() == 1 &&
	       (*i)->getSucc(0)->getDst() == *i)
	     continue; 

           mainGraph.push_back(new WTOInt(*i));   
         }

	 buildGraph(mainGraph, true);
         
	 if (mainGraph.size() == 0){
	   errs () << "WTO reporting\n";
	   errs () << "Program is trivially unsafe: consists of single basic block\n";
	   errs () << "ERROR Reachable\n";
	   errs () << "BRUNCH_STAT Result CEX\n";
	   exit(0);
	 }
	 
	 return mainGraph.front();
    }

    /** computes an interval graph from a given CFG, as per
      Aho, Sethi, Ullman 1986.
      This is continuously called until a graph of one node is computed (fixpoint)*/
    // XXX needs to delete previous graph from memory
    WTOInt* WTOPass::computeIntervals (WTOInt *i, Function &F, int &size)
    {
      WTOInt* head = i;
      
      std::vector<ip> headers;
      std::vector<WTOInt*> fixpoint;
      std::vector<WTOInt*> processed;
      std::vector<WTOInt*> intGraph;
      
     
      // -- if this is the first call to the function, create graph
      // -- that resembles CFG.
      if (head == NULL)
       head = createInitialGraph(F);

      
      WTOInt* newi = new WTOInt(*head);
      headers.push_back(ip(head,newi));
      
      while (!headers.empty()){
        fixpoint.clear();
        ip wtoPair = headers.back();
       
	WTOInt* h = wtoPair.first;
        WTOInt* hn = wtoPair.second;

	errs () << "===Looking at Header " << 
	  wtoPair.first->getHead()->getBB()->getName() << "\n\n";
        intGraph.push_back(hn);
        headers.pop_back();
        
        fixpoint.push_back(h);
        processed.push_back(h);
        
        // -- this is the first step in Aho, Sethi, Ullman (1986)
        // -- for each node hn, compute I(hn), see page 665
        firstLoop(fixpoint, processed, leftB, rightB, hn);
        errs () << "After first loop"; printIntervals(intGraph);       
        // -- this is the second step in Aho, Sethi, Ullman (1986)
        // -- adds remaining nodes to headers 
        // -- (those that have not been processed)
        secondLoop(fixpoint, processed, headers);
      }
      
      // -- add brackets to the interval graph
      setBrackets(intGraph,leftB,rightB);
      errs() << "Size of graph: " << intGraph.size() << "\n";
      printIntervals(intGraph);
      
      // -- connects nodes of the new graph
      buildGraph(intGraph, false);

      // -- return the size, we need to know the size to
      // -- detect fixpoint (i.e., when size = 1)
      size = intGraph.size(); 
      
      
      return newi;
    }
    
    /** checks if l is th head of a component */
    bool WTOPass::isWtoHead(const CutPoint &l) 
    {
      CutPointPtr cp = lbe->cutPts[F][l.getID ()];
      WTO* cpw = cp2wto[cp];
      return cpw->getComp ();
    }

    // -- Exits a component
    CutPointPtr WTOPass::nextWtoHead(const CutPoint &l) 
    {
      errs () << "Looking for head of " ;
      errs () << l.getID() << "\n";
      if (l.getID() >= 0)
      	errs () << "BB: " << l.getBB()->getName() << "\n";
      
      CutPointPtr cp = lbe->cutPts[F][l.getID ()];
      WTO* cpw = cp2wto[cp];
      
      WTO* p = cpw->getParent ();
      

      // -- find next child of root
      for (unsigned int i = 0; i < (p->getChildren ().size() - 1); ++i){
          
	if ((p->getChildren ()[i]->getCp ()->getID()) == 
	    cpw->getCp ()->getID()){
          return p->getChildren ()[i+1]->getCp ();
        }
      
      }
      
      errs () << "Head found: " ;
      errs () << p->getCp()->getID() << "\n";

      return p->getCp ();

    }
   
    /** traverses WTO recrusively from wr looking for cutpointptr cpl */
    bool inWto(WTO* wr, CutPointPtr cpl){
      if (cpl == wr->getCp ()) return true;
      
      for (size_t i = 0; i < wr->getChildren ().size(); ++i)
        if (inWto(wr->getChildren ()[i],cpl))
          return true;
      
      return false;  
    }
    
    /** checks if l is in component or subcomponent headed by ref */
    bool WTOPass::isInCompOrSub(const CutPoint &ref, const CutPoint &l) 
    {
      CutPointPtr cpr = lbe->cutPts[F][ref.getID ()];
      CutPointPtr cpl = lbe->cutPts[F][l.getID ()];

      WTO* wr = cp2wto[cpr];
      bool res = inWto(wr, cpl);
      return res;
    }
    
    /** gets cutpoint that comes after l in component */
    CutPointPtr WTOPass::wtoNextSame(const CutPoint &l) 
    {
      CutPointPtr cp = lbe->cutPts[F][l.getID ()];
      WTO* cpw = cp2wto[cp];

      // -- if component of form (b)
      if (cpw->getComp () && cpw->getChildren ().empty()) return cp;
      
      // -- otherwise if it has children
      if (!cpw->getChildren ().empty()) return cpw->getChildren ()[0]->getCp ();

      // -- otherwise it doesn't have children
      WTO* p = cpw->getParent ();
        
      // -- find next child of root
      for (unsigned int i = 0; i < (p->getChildren ().size() - 1); ++i){
        
        if (p->getChildren ()[i] == cpw)
          return p->getChildren ()[i+1]->getCp ();
      
      }

      return p->getCp ();
    }

    /** takes an interval graph and returns a (root of) tree of components */
    WTO* WTOPass::computeWTO(WTOInt* w){
     
      WTO* root = new WTO(leftB, NULL);

      int depth = 0;
      bool lookForHead = false;

      for (unsigned int i = 0; i < w->getInterval ().size(); ++i){
        // -- if left bracket, then we expect to see a head (lookForHead = t)
        if (w->getInterval ()[i]->getID() == -1){
          depth++;
          lookForHead = true;
          continue;
        }

        // -- if right bracket, then we go back to the parent of the current
        // -- component, since the bracket signifies the end of the component
        if (w->getInterval ()[i]->getID() == -2){
          depth--;
          root = root->getParent ();
          continue;
        }

        errs () << "Looking at " << w->getInterval ()[i]->getBB()->getName()  << "\n"; 
        
        // -- this creates an element in the WTO
        WTO* child = new WTO(w->getInterval ()[i], root);
        cp2wto[w->getInterval ()[i]] = child;
        child->setParent (*root);
        root->addChild(child);


        // -- in case it's the head of a component
        if (lookForHead){
          // -- ensure that there are no two left brackets in a row
          assert (w->getInterval ()[i]->getID() >= 0);
          
          // -- indicates that this is a head of comp
          child->setComp (true);
          
          root = child;
          lookForHead = false;
        }
      }
      return root;
    }

    void WTOPass::deleteGraph(WTOInt* g){
      for (size_t i = 0; i < g->getSuccs().size(); ++i){
        deleteGraph(g->getSuccs()[i]);
      }
      
      free(g);
    }
    
    /** main function for computing a weak topo order */
    void WTOPass::weakTopologicalOrder(Function* F){
        // -- these are used as left and right brackets
        // -- to identify components (cycles)
        leftB.reset(new CutPoint(-1,NULL));
        rightB.reset(new CutPoint(-2,NULL));

        int size;
        int sizeN = 0;

        WTOInt* graph = computeIntervals(NULL, *F, size);
        
        // -- fixpoint computation for interval graph
        while (size > 1){
          graph = computeIntervals(graph, *F, sizeN);

          // -- if size stabilizes before 1, then irreducible CFG
          assert ((sizeN != size) &&"\n= GRAPH IRREDUCIBLE =");

          size = sizeN;
        }
        
	// -- add self loops
	addSelfLoops(*F, graph); 

        // -- Finally, add the return block
        graph->getInterval ().push_back(lbe->cutPts[F][1]);
       
        wtoOrder = graph;
       
	// -- print out final interval
	std::vector<WTOInt*> fInts;
	fInts.push_back(graph);
        printIntervals(fInts);

        // -- Now get a WTO from the interval graph
        computeWTO(wtoOrder);
    }

   
    bool WTOPass::runOnModule (Module &M) {
        F = M.getFunction("main");
        lbe = &(getAnalysis<LBE>());
        weakTopologicalOrder(F);
        return false;
    }

char WTOPass::ID = 0;
INITIALIZE_PASS_BEGIN(WTOPass, "WTOPass", 
		      "Weak Topological Ordering", 
		      false, false)
INITIALIZE_PASS_DEPENDENCY(LBE)
INITIALIZE_PASS_DEPENDENCY(Topo)
INITIALIZE_PASS_DEPENDENCY(UnifyFunctionExitNodes)
INITIALIZE_PASS_END(WTOPass, "WTOPass", 
		    "Weak Topological Ordering", 
		    false, false)
